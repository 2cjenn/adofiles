postutil clear
capture program drop fitdata
program define fitdata
	syntax [, 		///
			INfile(string)					/// -Name of data files-
			POSTdir(string)					/// -Directory to output point estimates to-
			NRUNS(int 1000)					/// -Number of runs of the simulation-
			NTrans(int 1)					/// -Number of transitions-
			MAXTime(string)					///
			COVARiates(string)				/// -Baseline covariates-
			TRANSMATrix(name)				/// -Transition matrix for the multistate system-
			MONTHS(numlist)					/// -List of timepoints at which to record point estimates-
			FLEXdf(numlist)					/// -List of degrees of freedom for flexible parametric models-
			CI								/// -Should confidence intervals be calculated?-
			DIFFerence							/// -Calculate the difference between the ats?
			TDEdf(numlist)					///
											///
			*								/// -ats for predictms-
		]
	local predictats `options'
	local ats `options'
	local atind = 1
	while "`ats'"!="" {
		local 0 , `ats'
		syntax , [at`atind'(string) *]
		local ats `options'
		local atind = `atind'+1
	}
	local atcount = max(`atind' - 1, 1)
	
	if "`transmatrix'" == "" {
		mat transmatrix = (.,1,2\.,.,3\.,.,.)
	}
		
	foreach df of local flexdf {
		local models = "`models' rp`df'"
	}
	local collist = ""
	foreach month of local months {
		foreach model of local models { 
			forvalues tr=1/`ntrans' {
				forvalues at=1/`atcount' {
						local collist = "`collist' `model'_`month'_at`at'_`tr' `model'_`month'_at`at'_`tr'_lci `model'_`month'_at`at'_`tr'_uci"
				}
				if "`difference'" != "" {
					local collist = "`collist' diff_`model'_`month'_`tr' diff_`model'_`month'_`tr'_lci diff_`model'_`month'_`tr'_uci"
				}
			}
		}
	}
	
	if "`difference'" != "" {
		local postdir = "`postdir'diff_"
	}
	
	tempname transprob
	postfile `transprob' `collist' using `postdir'transprob, replace
	
	tempname los
	postfile `los' `collist' using `postdir'los, replace
	
	tempname abic
	postfile `abic' run_no trans str5 model number ll df AIC BIC using `postdir'ABIC, replace
	
	tempname convergence
	postfile `convergence' run_no df tr1 tr2 tr3 using `postdir'convergence, replace
	
	
	constraint drop _all
	
	forvalues i=1/`nruns' {
		display "Run number `i'"
		use `infile'`i', clear
		estimates clear
		* Fit models to each transition
		local postpar = ""
		
		* Flexible parametric
		foreach df of local flexdf {
			local rp`df'_conv = 0
			local postconv "(`i') (`df')"
			forvalues tr = 1/3 {
				capture qui stpm2 `covariates' if _trans==`tr', scale(h) df(`df') iter(100) failconvlininit
				if (_rc != 0 | e(converged) != 1) {
					di "m`tr'_rp`df' did not converge"
					local rp`df'_conv = 1
					local postconv "`postconv' (0)"
				} 
				else {
					est store m`tr'_rp`df'
					local postconv "`postconv' (1)"
				}
				//display "Done rp`df'"
			}
			post `convergence' `postconv'
		}
		
		* Record AIC and BIC
		postAIC, posting(`abic') ntrans(`ntrans') models(`models') runno(`i')
		
		* Make predictions of transition probabilities and length of stay
		capture drop tt
		range tt 0 `maxtime' 1001

		foreach model of local models {
			display "`model'"
			// If all models converged, try to predict:
			if ``model'_conv' == 0 {
				capture qui predictms, transmatrix(transmatrix) timevar(tt) aj los ///
					models(m1_`model' m2_`model' m3_`model') `ci' `predictats' `difference'

				// If there's an error when predicting
				if (_rc != 0) {
					// And you were trying to get confidence intervals
					if "`ci'" != "" {
						// Make empty confidence intervals
						forvalues tr=1/`ntrans' { 
							forvalues at=1/`atcount' {
								gen p_`model'_at`at'_`tr'_lci = .
								gen p_`model'_at`at'_`tr'_uci = .
								gen los_`model'_at`at'_`tr'_lci = .
								gen los_`model'_at`at'_`tr'_uci = .
							}
							quietly {
								gen pdiff_`model'_`tr'_lci = .
								gen pdiff_`model'_`tr'_uci = .
								gen ldiff_`model'_`tr'_lci = .
								gen ldiff_`model'_`tr'_uci = .
							}
						}
						// And try again without confidence intervals
						capture qui predictms, transmatrix(transmatrix) timevar(tt) aj los ///
							models(m1_`model' m2_`model' m3_`model') `predictats' `difference'
						// If there's still an error
						if (_rc != 0) {
							di "Couldn't predict values"
							// Give up and make empty point estimates
							forvalues tr=1/`ntrans' { 
								forvalues at=1/`atcount' {
									// Generate empty variables
									gen p_`model'_at`at'_`tr' = .
									gen los_`model'_at`at'_`tr' = .
								}
								if "`difference'" != "" {
									gen pdiff_`model'_`tr' = .
									gen ldiff_`model'_`tr' = .
								}
							}
						}
						// If there isn't an error, you just have point estimates
						else {
							display "ConfInt failed but Estimates recorded"
							forvalues at=1/`atcount' {
								rename _prob_at`at'_1_* p_`model'_at`at'_*
								rename _los_at`at'_1_* los_`model'_at`at'_*
							}
							if "`difference'" != "" {
								rename _diff_prob_at`atcount'_1_* pdiff_`model'_*
								rename _diff_los_at`atcount'_1_* ldiff_`model'_*
							}
						}
					}
					// If you weren't trying to get confidence intervals
					else {
						// Give up and make a bunch of empty values
						di "Couldn't predict values"
						forvalues tr=1/`ntrans' { 
							forvalues at=1/`atcount' {
								// Generate empty variables
								gen p_`model'_at`at'_`tr' = .
								gen los_`model'_at`at'_`tr' = .
								
								gen p_`model'_at`at'_`tr'_lci = .
								gen p_`model'_at`at'_`tr'_uci = .
								gen los_`model'_at`at'_`tr'_lci = .
								gen los_`model'_at`at'_`tr'_uci = .
							}
							if "`difference'" != "" {
								gen pdiff_`model'_`tr' = .
								gen ldiff_`model'_`tr' = .
								quietly {
									gen pdiff_`model'_`tr'_lci = .
									gen pdiff_`model'_`tr'_uci = .
									gen ldiff_`model'_`tr'_lci = .
									gen ldiff_`model'_`tr'_uci = .
								}
							}
						}
					}
				} 
				// If there isn't an error then yay!
				else {
					forvalues at=1/`atcount' {
						rename _prob_at`at'_1_* p_`model'_at`at'_*
						rename _los_at`at'_1_* los_`model'_at`at'_*
						
						if "`ci'" == "" {
							quietly {
								forvalues tr=1/`ntrans' { 
									gen p_`model'_at`at'_`tr'_lci = .
									gen p_`model'_at`at'_`tr'_uci = .
									gen los_`model'_at`at'_`tr'_lci = .
									gen los_`model'_at`at'_`tr'_uci = .
								}
							}
						}
					}
					if "`difference'" != "" {
						rename _diff_prob_at`atcount'_1_* pdiff_`model'_*
						rename _diff_los_at`atcount'_1_* ldiff_`model'_*
						if "`ci'" == "" {
							quietly {
								forvalues tr=1/`ntrans' { 
									gen pdiff_`model'_`tr'_lci = .
									gen pdiff_`model'_`tr'_uci = .
									gen ldiff_`model'_`tr'_lci = .
									gen ldiff_`model'_`tr'_uci = .
								}
							}
						}
					}
					
				}
				constraint drop _all
				/*
				forvalues tr = 1/`ntrans' {
					estimates restore m`tr'_`model'
					predict h`tr'_`model', hazard timevar(tt)
				}
				*/
			}
			// If not all models converged, can't predict so just make empties
			else { 
				// Generate empty variables
				forvalues tr=1/`ntrans' {
					forvalues at=1/`atcount' {
						gen p_`model'_at`at'_`tr' = .
						gen los_`model'_at`at'_`tr' = .
						
						quietly {
							gen p_`model'_at`at'_`tr'_lci = .
							gen p_`model'_at`at'_`tr'_uci = .
							gen los_`model'_at`at'_`tr'_lci = .
							gen los_`model'_at`at'_`tr'_uci = .
						}
					}
					if "`difference'" != "" {
						gen pdiff_`model'_`tr' = .
						gen ldiff_`model'_`tr' = .
						quietly {
							gen pdiff_`model'_`tr'_lci = .
							gen pdiff_`model'_`tr'_uci = .
							gen ldiff_`model'_`tr'_lci = .
							gen ldiff_`model'_`tr'_uci = .
						}
					}
				}
			}
		}
		
		* Post the point estimates
		local postprob = ""
		local postlength = ""
		local postpar = ""
		foreach month of local months {
			foreach model of local models {
				forvalues tr=1/`ntrans' {
					forvalues at=1/`atcount' {
						qui summ p_`model'_at`at'_`tr' if tt==`month'/12
						local probmean = r(mean)
						qui summ los_`model'_at`at'_`tr' if tt==`month'/12
						local losmean = r(mean)
						
						qui summ p_`model'_at`at'_`tr'_lci if tt==`month'/12
						local probmeanl = r(mean)
						qui summ p_`model'_at`at'_`tr'_uci if tt==`month'/12
						local probmeanu = r(mean)
						
						qui summ los_`model'_at`at'_`tr'_lci if tt==`month'/12
						local losmeanl = r(mean)
						qui summ los_`model'_at`at'_`tr'_uci if tt==`month'/12
						local losmeanu = r(mean)
						
						local postprob = "`postprob' (`probmean') (`probmeanl') (`probmeanu')"
						local postlength = "`postlength' (`losmean') (`losmeanl') (`losmeanu')"
					}
					if "`difference'" != "" {
						qui summ pdiff_`model'_`tr' if tt==`month'/12
						local probdiff = r(mean)
						qui summ ldiff_`model'_`tr' if tt==`month'/12
						local losdiff = r(mean)
						
						qui summ pdiff_`model'_`tr'_lci if tt==`month'/12
						local probdiffl = r(mean)
						qui summ pdiff_`model'_`tr'_uci if tt==`month'/12
						local probdiffu = r(mean)
						
						qui summ ldiff_`model'_`tr'_lci if tt==`month'/12
						local losdiffl = r(mean)
						qui summ ldiff_`model'_`tr'_uci if tt==`month'/12
						local losdiffu = r(mean)
						
						local postprob = "`postprob' (`probdiff') (`probdiffl') (`probdiffu')"
						local postlength = "`postlength' (`losdiff') (`losdiffl') (`losdiffu')"
					}
				}
			}
		}
		post `transprob' `postprob'
		post `los' `postlength'
		save `infile'`i'_fit, replace
	}
	postclose `transprob'
	postclose `los'
	postclose `abic'
	postclose `convergence'
	
	postutil clear
end


