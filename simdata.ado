* Run the simulation x times, save the simulated data
capture program drop simdata
program define simdata
	syntax [, 		///
		OUTfile(string)					///
		NRUNS(int 1000)					/// -Number of runs of the simulation-
		OBServations(int 1000)			///
		SEED(int 398894)				///
		NTrans(int 1)					/// -Number of transitions to simulate-
		GAMMAS1(numlist min=1)			/// -Gamma for first half of mixture Weibull for each transition-
		LAMBDAS1(numlist min=1)			/// -Lambda for first half of mixture Weibull for each transition-
		GAMMAS2(numlist min=1)			/// -Gamma for second half of mixture Weibull for each transition-
		LAMBDAS2(numlist min=1)			/// -Lambda for second half of mixture Weibull for each transition-
		PMIX(numlist min=1)				/// -Mixture parameter-
		MAXTime(string)					/// -Maximum simulated time-
		TRANSMATrix(name)				/// -Transition matrix for the multistate system-
										/// 
		*								/// -Infinite covariates-
	]

	set seed `seed'
	
	// Parse covar
	local covartext `options'
	local covariates `options'
	local covarind = 1
	while "`covariates'"!="" {
		local 0 , `covariates'
		syntax , [covar`covarind'(string) *]
		local covariates `options'

		tokenize "`covar`covarind''"
		cap confirm string `2'
		local covarlist = "`covarlist' `2'"

		local covarind = `covarind'+1
	}
	
	forvalues i=1/`nruns' {
		qui illmixweib, obs(`observations') ntrans(3) pmix(`pmix') maxtime(`maxtime') ///
			gammas1(`gammas1') lambdas1(`lambdas1') ///
			gammas2(`gammas2') lambdas2(`lambdas2') ///
			`covartext'
		
		mat tmat = (.,1,2\.,.,3\.,.,.)
		qui msset, id(id) transmat(tmat) states(illness death) ///
			times(itime dtime) covariates(`covarlist')
		
		matrix freqmat = r(freqmatrix)
		
		qui stset _stop, enter(_start) failure(_status=1) scale(1)
		
		save `outfile'`i', replace
	}
end
