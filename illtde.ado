capture program drop illtde
program define illtde, rclass
	syntax 	[, 							///
			OBServations(int 1000)		///
			TRUEvals(numlist)			/// -List of timepoints at which to simulate point estimates-
			DIFFERENCE					///
			POSTdir(string)				///
			SEED(numlist int max=1)		///
			NTrans(int 1)				/// -Number of transitions to simulate-
			GAMMAS1(numlist min=1)		/// -Gamma for first half of mixture Weibull for each transition-
			LAMBDAS1(numlist min=1)		/// -Lambda for first half of mixture Weibull for each transition-
			GAMMAS2(numlist min=1)		/// -Gamma for second half of mixture Weibull for each transition-
			LAMBDAS2(numlist min=1)		/// -Lambda for second half of mixture Weibull for each transition-
			PMIX(numlist min=1)			/// -Mixture parameter-
			MAXTime(string)				/// -Maximum simulated time-
			TRANSMATrix(name)			/// -Transition matrix for the multistate system-
			TDE(string)					/// -Time dependent effects-
										///	
			*							/// -Infinite covariates-
			]
			
	// Prep
	
	forvalues i=1/`ntrans' {
		local l1`i' : word `i' of `lambdas1'
		local g1`i' : word `i' of `gammas1'
		local l2`i' : word `i' of `lambdas2'
		local g2`i' : word `i' of `gammas2'
		local p`i' : word `i' of `pmix'
		local tde`i' : word `i' of `tde'
	}
	
	clear
	set obs `observations'
	// make id column
	gen id = _n
	//set seed for reproducibility
	if "`seed'" != "" {
		set seed `seed'
	}
	
	// Parse covar
	local covariates `options'
	local covarind = 1
	while "`covariates'"!="" {
		local 0 , `covariates'
		syntax , [covar`covarind'(string) *]
		local covariates `options'
		local varcount : word count `covar`covarind''
		if `varcount' != `ntrans' + 2 {
			di as error "Something missing from specification of covariates""
			exit 198
		}
		tokenize "`covar`covarind''"
		cap confirm num `1'
		cap confirm string `2'
		local covarlist = "`covarlist' `2'"
		if inrange(`1', 0, 1) {
			gen `2' = runiform() < `1'
		}
		else {
			di as error "Binary covariates should have a probability between 0 and 1"
			exit 198
		}
		
		forvalues i = 1/`ntrans' {
			local j = `i' + 2
			local covariate`i' = "`covariate`i'' `2' ``j''"
			local covar`i' = "`2':*``j''"
			display "`covar`i''"
		}
		local covarind = `covarind'+1
		
	}
	
	local tr = 1
	local loghazard "ln((`p`tr'':*`l1`tr'':*`g1`tr'':*(#t:^(`g1`tr'':-1)):*exp(-`l1`tr'':*(#t:^`g1`tr'')) :+ (1:-`p`tr''):*`l2`tr'':*`g2`tr'':*(#t:^(`g2`tr'':-1)):*exp(-`l2`tr'':*(#t:^`g2`tr''))) :/ (`p`tr'':*exp(-`l1`tr'':*(#t:^`g1`tr'')) + (1-`p`tr''):*exp(-`l2`tr'':*(#t:^`g2`tr''))))"
	local loghaz "`loghazard' :+ `covar`tr'' :+ `tde`tr''"
	display "`loghaz'"
	survsimdelay itime illness, ///
	loghazard(`loghaz') maxtime(`maxtime') nodes(100) covariates(`covariate1')
	
	// simulate times for second transition (healthy -> death)
	local tr = 2
	local loghazard "ln((`p`tr'':*`l1`tr'':*`g1`tr'':*(#t:^(`g1`tr'':-1)):*exp(-`l1`tr'':*(#t:^`g1`tr'')) :+ (1:-`p`tr''):*`l2`tr'':*`g2`tr'':*(#t:^(`g2`tr'':-1)):*exp(-`l2`tr'':*(#t:^`g2`tr''))) :/ (`p`tr'':*exp(-`l1`tr'':*(#t:^`g1`tr'')) + (1-`p`tr''):*exp(-`l2`tr'':*(#t:^`g2`tr''))))"
	local loghaz "`loghazard' :+ `covar`tr'' :+ `tde`tr''"
	display "`loghaz'"
	survsimdelay dtime death,  ///
	loghazard(`loghaz') maxtime(`maxtime') nodes(100) covariates(`covariate2')
	// those two were competing risks, allocate which one happened
	* death happened first
	replace illness = 0 if itime > dtime
	replace itime = . if itime > dtime
	* illness happened first
	replace death = 0 if dtime > itime
	replace dtime = . if dtime > itime

	// simulate for third transition (illness -> death)
	// - only happens if in illness state
	local tr = 3
	local loghazard "ln((`p`tr'':*`l1`tr'':*`g1`tr'':*(#t:^(`g1`tr'':-1)):*exp(-`l1`tr'':*(#t:^`g1`tr'')) :+ (1:-`p`tr''):*`l2`tr'':*`g2`tr'':*(#t:^(`g2`tr'':-1)):*exp(-`l2`tr'':*(#t:^`g2`tr''))) :/ (`p`tr'':*exp(-`l1`tr'':*(#t:^`g1`tr'')) + (1-`p`tr''):*exp(-`l2`tr'':*(#t:^`g2`tr''))))"
	local loghaz "`loghazard' :+ `covar`tr'' :+ `tde`tr''"
	display "`loghaz'"
	survsimdelay stime3 event3, enter(itime) ///
	loghazard(`loghaz') maxtime(`maxtime') nodes(100) covariates(`covariate3') 
	* function requires maxtime() 
	* but now max time is <15 and different for each individual
	// incorporate illness->death into death times
	replace dtime = stime3 if illness == 1
	replace death = 1 if illness == 1 & dtime < `maxtime'
	drop stime3 event3 _survsim_rc

	// To avoid lots of times < 1 day, min survival is 1 day
	replace itime = 0.00274 if itime < 0.00274
	replace dtime = 0.00274 if dtime < 0.00274
	
	// Truevals
	if "`truevals'" != "" {
		local collist = ""
		foreach month of local truevals {
			foreach state in healthy ill dead {
				local collist = "`collist' `state'_`month'"
			}
		}
		tempname transprob
		tempname los
		
		if "`difference'" != "" {
			foreach covar of local covarlist {
				postfile `transprob' `collist' using `postdir'trueprob_`covar'diff, replace
				postfile `los' `collist' using `postdir'truelos_`covar'diff, replace
				local postprob = ""
				local postlength = ""
				
				forvalues val = 0/1 {
					frame copy default trt`val'
					frame change trt`val'
					drop if `covar' != `val'
					frame change default
				}
				foreach month of local truevals {
					local t = `month'/12
					forvalues val = 0/1 {
						frame change trt`val'
						local newobs = _N
						quietly {
							* Probability of being in each state at time t
							count if itime>=`t' & dtime>=`t' 
							local Phealthy`val' `r(N)'/`newobs'
							count if itime<`t' & dtime>=`t'
							local Pill`val' `r(N)'/`newobs'
							count if dtime<`t'
							local Pdead`val' `r(N)'/`newobs'
							
							
							* Length of time each individual spent in each state at time t
							gen h`month' = min(itime, dtime, `t')
							gen i`month' = round(min(dtime, `t') - h`month', 0.0000001)
							gen d`month' = `t' - min(dtime, `t')
							
							* Length of stay spent in each state at time t
							sum h`month'
							local Lhealthy`val' `r(mean)'
							sum i`month'
							local Lill`val' `r(mean)'
							sum d`month'
							local Ldead`val' `r(mean)'
							
						}
						frame change default
					}
					display "`month' `Phealthy1' `Phealthy0'"
					local postprob = "`postprob' (`Phealthy1'-`Phealthy0') (`Pill1'-`Pill0') (`Pdead1'-`Pdead0')"
					local postlos = "`postlos' (`Lhealthy1'-`Lhealthy0') (`Lill1'-`Lill0') (`Ldead1'-`Ldead0')"
				}
				post `transprob' `postprob'
				postclose `transprob'
				
				post `los' `postlos'
				postclose `los'
				
				postutil clear
				forvalues val = 0/1 {
					frame drop trt`val'
				}
			}
		}
		else {
			postfile `transprob' `collist' using `postdir'trueprob, replace
			postfile `los' `collist' using `postdir'truelos, replace
			local postprob = ""
			local postlength = ""
			
			if "`covarlist'" != ""{
				foreach covar of local covarlist {
					drop if `covar' != 0
				}
			}			
			local newobs = _N
			
			foreach month of local truevals {
				local t = `month'/12
				quietly {
					* Probability of being in each state at time t
					count if itime>=`t' & dtime>=`t' 
					local Phealthy `r(N)'/`newobs'
					count if itime<`t' & dtime>=`t'
					local Pill `r(N)'/`newobs'
					count if dtime<`t'
					local Pdead `r(N)'/`newobs'
					local postprob = "`postprob' (`Phealthy') (`Pill') (`Pdead')"
					
					* Length of time each individual spent in each state at time t
					gen h`month' = min(itime, dtime, `t')
					gen i`month' = round(min(dtime, `t') - h`month', 0.0000001)
					gen d`month' = `t' - min(dtime, `t')
					
					* Length of stay spent in each state at time t
					sum h`month'
					local Lhealthy `r(mean)'
					sum i`month'
					local Lill `r(mean)'
					sum d`month'
					local Ldead `r(mean)'
					local postlos = "`postlos' (`Lhealthy') (`Lill') (`Ldead')"
				}
			}
			post `transprob' `postprob'
			postclose `transprob'
			
			post `los' `postlos'
			postclose `los'
			
			postutil clear
		}
	}
	else {	
		mat tmat = (.,1,2\.,.,3\.,.,.)
		qui msset, id(id) transmat(tmat) states(illness death) ///
			times(itime dtime) covariates(`covarlist')
		
		matrix freqmat = r(freqmatrix)
		
		qui stset _stop, enter(_start) failure(_status=1) scale(1)
		
		//msaj, transmat(tmat) id(id)
		//twoway (line P_AJ* _t,sort connect(stepstair))
	}
end


