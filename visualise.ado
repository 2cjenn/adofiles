* Calculate survival function for different mixture Weibulls
capture program drop mixweibsurv
program define mixweibsurv
	syntax newvarlist(min=1), 		///
			[						///
			TIMEvar(varname)		///
			L1(real 0.1)			///
			G1(real 0.1)			///
			L2(real 0.1)			///
			G2(real 0.1)			///
			Pmix(real 0.5)			///
			LOGHR(numlist min=1)	/// -Log hazard ratio of binary covariate-
			TDE(string)				///
			]
	tokenize `varlist'
	gen `1' = `pmix'*exp(-`l1'*(`timevar'^`g1')) + (1-`pmix')*exp(-`l2'*(`timevar'^`g2'))
	if "`loghr'" != "" | "`tde'" != "" {
		gen `2' = `1'
		if "`loghr'" != "" {
			replace `2' = `2' ^ exp(`loghr')
			display "`loghr'"
		}
		if "`tde'" != "" {
			replace `2' = `2' ^ exp(`tde')
			display "`tde'"
		}
	}
end

* Calculate hazard function for different mixture Weibulls
capture program drop mixweibhaz
program define mixweibhaz
	syntax newvarlist(min=1), 		///
			[						///
			TIMEvar(varname)		///
			L1(real 0.1)			///
			G1(real 0.1)			///
			L2(real 0.1)			///
			G2(real 0.1)			///
			Pmix(real 0.5)			///
			LOGHR(numlist min=1)	/// -Log hazard ratio of binary covariate-
			TDE(string)				///
			]
	tokenize `varlist'
	gen `1' = (`l1'*`g1'*(`timevar'^(`g1'-1))*`pmix'*exp(-`l1'*(`timevar'^`g1'))	///
			+ `l2'*`g2'*(`timevar'^(`g2'-1))*(1-`pmix')*exp(-`l2'*(`timevar'^`g2')))	///
			/ (`pmix'*exp(-`l1'*(`timevar'^`g1'))+(1-`pmix')*exp(-`l2'*(`timevar'^`g2')))
	if "`loghr'" != "" | "`tde'" != "" {
		gen `2' = `1'
		if "`loghr'" != "" {
			replace `2' = `2' * exp(`loghr')
		}
		if "`tde'" != "" {
			replace `2' = `2' * exp(`tde')
		}
	}
end

* Make graphs of hazard and survival functions
capture program drop visualise
program define visualise
	syntax 	[, 						///
			SEED(numlist int max=1)	///
			NTrans(int 1)			/// -Number of transitions to simulate-
			GAMMAS1(numlist min=1)	/// -Gamma for first half of mixture Weibull for each transition-
			LAMBDAS1(numlist min=1)	/// -Lambda for first half of mixture Weibull for each transition-
			GAMMAS2(numlist min=1)	/// -Gamma for second half of mixture Weibull for each transition-
			LAMBDAS2(numlist min=1)	/// -Lambda for second half of mixture Weibull for each transition-
			PMIX(numlist min=1)		/// -Mixture parameter-
			MAXTime(string)			/// -Maximum simulated time-
			LOGHR(numlist min=1)	/// -Log hazard ratio of binary covariate-
			TDE(string)				///
			OUTDIR(string)			///
			SIMULATION(int 1)		///
			PLOTAJ					///
			]
			
	forvalues i=1/`ntrans' {
		local l1`i' : word `i' of `lambdas1'
		local g1`i' : word `i' of `gammas1'
		local l2`i' : word `i' of `lambdas2'
		local g2`i' : word `i' of `gammas2'
		local p`i' : word `i' of `pmix'
		local loghr`i' : word `i' of `loghr'
		local tde`i' : word `i' of `tde'
	}
	
	clear
	set obs 1001
	
	capture drop tt
	range tt 0 `maxtime' 1001
	
	capture drop haz*
	capture drop surv*
	forvalues tr=1/`ntrans' {	
		if "`loghr`tr''" != "" & "`tde`tr''" != "" {
			mixweibhaz haz`tr' haz`tr'_at1, timevar(tt) l1(`l1`tr'') g1(`g1`tr'') ///
				l2(`l2`tr'') g2(`g2`tr'') pmix(`p`tr'') loghr(`loghr`tr'') tde(`tde`tr'')
			line haz`tr' haz`tr'_at1 tt, ///
			xtitle("Time, years") ytitle("Hazard") title("Transition `tr'") ///
			legend(label(1 "Treatment==0") label(2 "Treatment==1")) name(haz`tr', replace) nodraw
			
			mixweibsurv surv`tr' surv`tr'_at1, timevar(tt) l1(`l1`tr'') g1(`g1`tr'') ///
				l2(`l2`tr'') g2(`g2`tr'') pmix(`p`tr'') loghr(`loghr`tr'') tde(`tde`tr'')
			line surv`tr' surv`tr'_at1 tt, ///
			xtitle("Time, years") ytitle("Survival function") title("Transition `tr'") ///
			legend(label(1 "Treatment==0") label(2 "Treatment==1")) name(surv`tr', replace) nodraw
			
			gen hazrat`tr' = haz`tr'_at1/haz`tr'
			line hazrat`tr' tt, ///
			xtitle("Time, years") ytitle("Hazard ratio") title("Transition `tr'") ///
			name(hazrat`tr', replace) nodraw
			
		}
		else if "`loghr`tr''" != "" {
			mixweibhaz haz`tr' haz`tr'_at1, timevar(tt) l1(`l1`tr'') g1(`g1`tr'') ///
				l2(`l2`tr'') g2(`g2`tr'') pmix(`p`tr'') loghr(`loghr`tr'')
			line haz`tr' haz`tr'_at1 tt, ///
			xtitle("Time, years") ytitle("Hazard") title("Transition `tr'") ///
			legend(label(1 "Treatment==0") label(2 "Treatment==1")) name(haz`tr', replace) nodraw
			
			mixweibsurv surv`tr' surv`tr'_at1, timevar(tt) l1(`l1`tr'') g1(`g1`tr'') ///
				l2(`l2`tr'') g2(`g2`tr'') pmix(`p`tr'') loghr(`loghr`tr'')
			line surv`tr' surv`tr'_at1 tt, ///
			xtitle("Time, years") ytitle("Survival function") title("Transition `tr'") ///
			legend(label(1 "Treatment==0") label(2 "Treatment==1")) name(surv`tr', replace) nodraw
		}
		else if "`tde`tr''" != "" {
			mixweibhaz haz`tr' haz`tr'_at1, timevar(tt) l1(`l1`tr'') g1(`g1`tr'') ///
				l2(`l2`tr'') g2(`g2`tr'') pmix(`p`tr'') tde(`tde`tr'')
			line haz`tr' haz`tr'_at1 tt, ///
			xtitle("Time, years") ytitle("Hazard") title("Transition `tr'") ///
			legend(label(1 "Treatment==0") label(2 "Treatment==1")) name(haz`tr', replace) nodraw
			
			mixweibsurv surv`tr' surv`tr'_at1, timevar(tt) l1(`l1`tr'') g1(`g1`tr'') ///
				l2(`l2`tr'') g2(`g2`tr'') pmix(`p`tr'') tde(`tde`tr'')
			line surv`tr' surv`tr'_at1 tt, ///
			xtitle("Time, years") ytitle("Survival function") title("Transition `tr'") ///
			legend(label(1 "Treatment==0") label(2 "Treatment==1")) name(surv`tr', replace) nodraw
		}
		else if "`tde`tr''" == "" & "`loghr`tr''" == "" {
			mixweibhaz haz`tr', timevar(tt) l1(`l1`tr'') g1(`g1`tr'') ///
				l2(`l2`tr'') g2(`g2`tr'') pmix(`p`tr'')
			line haz`tr' tt, ///
			xtitle("Time, years") ytitle("Hazard") title("Transition `tr'") ///
			legend(label(1 "Hazard")) name(haz`tr', replace) nodraw
			
			mixweibsurv surv`tr', timevar(tt) l1(`l1`tr'') g1(`g1`tr'') ///
				l2(`l2`tr'') g2(`g2`tr'') pmix(`p`tr'')
			line surv`tr' tt, ///
			xtitle("Time, years") ytitle("Survival function") title("Transition `tr'") ///
			legend(label(1 "Survival")) name(surv`tr', replace) nodraw
		}
	}
	grc1leg2 haz1 haz2 haz3, ycommon legendfrom(haz1) ///
	title("Hazard functions for Scenario `simulation'") name(hazard, replace)
	graph export "`outdir'truehazard.png", replace
	grc1leg2 surv1 surv2 surv3, ycommon legendfrom(surv1) ///
	title("Survival functions for Scenario `simulation'") name(survival, replace)
	graph export "`outdir'truesurvival.png", replace
	if "`loghr`tr''" != "" & "`tde`tr''" != "" {
		graph combine hazrat1 hazrat2 hazrat3, ycommon ///
		title("Hazard ratios for Scenario `simulation'") name(hazratio, replace)
		graph export "`outdir'truehazrat.png", replace
	}
	
	if "`plotaj'" != "" {
		simdata, outfile("Stata\Simulations\Sim1\test") nruns(1) obs(10000) ///
			seed(398894) ntrans(3) pmix(`p1' `p2' `p3') maxtime(5) ///
			gammas1(`g11' `g12' `g13') lambdas1(`l11' `l12' `l13') ///
			gammas2(`g21' `g22' `g23') lambdas2(`l21' `l22' `l23')
			
		msaj, transmat(tmat) id(id)

		twoway (line P_AJ* _t, sort connect(stepstair)) , 		///
			title("Aalen-Johansen estimates of transition probabilities")		///
			legend(order(1 "Healthy" 2 "Sick" 3 "Death"))	///
			xtitle("Follow-up time (years)") ytitle("Transition probability ") ///
			ylab(0.0(0.2)1.0) xlab(0(1)`maxtime') name(AJ, replace)
		graph export "`outdir'AJ.png", replace
	}
end
