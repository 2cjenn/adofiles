* STATA DO FILE
* Project
* Multistate course - Acute myeloid leukaemia data
* 04/06/2019

* set Stata version
version 15.1

* start logging
capture log using "log.log", append

* set working directory
capture cd "C:\Users\infin\Documents\Leicester\Project"

********************************************************************************
* 
*-------------------------------------------------------------------------------
* Load in the data
*-------------------------------------------------------------------------------
use "Data\myeloid", clear

* Make treatment a factor
encode trt, gen(Treatment)
encode trt, gen(trtf)
levels(Treatment)

* First, simple two-state time-to-death
stset futime, failure(death) scale(365.25)

* Check for difference between treatments
sts test trtf

sts graph, ci risktable ///
xtitle("Survival time, years") ytitle("Survival function") ///
saving("Writeup\Graphs\Myeloid\KaplanMeier", replace)
graph export "Writeup\Graphs\Myeloid\KaplanMeier.png", replace

sts graph, by(Treatment) ci risktable ///
xtitle("Survival time, years") ytitle("Survival function") ///
title("Kaplan-Meier survival estimates by treatment")
graph export "Writeup\Graphs\Myeloid\KaplanMeierTrt.png", replace

* Weibull
streg i.trtf, dist(weibull) nolog
capture drop wb
predict wb, surv
predict h, haz

sts graph, by(trtf) ///
addplot(line wb _t if trtf==1, sort color(blue) lpattern(dash) || ///
line wb _t if trtf==2, sort color(red) lpattern(dash) ///
legend(order(1 "Treatment A" 2 "Treatment B")) ///
xtitle("Time, years") ytitle("Survival proportion")) 
graph export "Writeup\Graphs\Myeloid\Weibull.png", replace

sts graph, hazard by(trtf) kernel(epan2) ytitle("Hazard") ///
legend(label(1 "Treatment A") label(2 "Treatment B")) 

twoway (line h _t if trtf==1, sort color(blue)) (line h _t if trtf==2, sort color(red) lpattern(dash)), legend(label(1 "Treatment A") label(2 "Treatment B")) ///
title("Predicted hazard functions") xtitle("Time, years") ytitle("Predicted hazard")
graph export "Writeup\Graphs\Myeloid\Hazard.png", replace

* Cox
stcox i.trtf

sts gen surv=s, by(trtf)

stcurve, surv at1(trtf=1) at2(trtf=2) ///
addplot(line surv _t if trtf==1, sort color(blue) lpattern(dash) || ///
line surv _t if trtf==2, sort color(red) lpattern(dash) ///
xtitle("Time, years") legend(order(1 "Treatment A" 2 "Treatment B")))
graph export "Writeup\Graphs\Myeloid\CoxTrt.png", replace

sts graph, by(trtf) ///
legend(label(1 "Treatment A") label(2 "Treatment B")) ///
addplot(line wb _t if trtf==1, sort color(blue) lpattern(dash)|| line wb _t if trtf==2, sort color(red) lpattern(dash)) 

* Flexible Parametric Models
capture est drop m*
capture drop haz*
capture drop surv*
local graphhaz = ""
local graphsurv = ""
forvalues i=1/10{
	stpm2 trtf, scale(h) df(`i') failconvlininit
	est store m`i'
	* eststo: est stats m`i'
	predict haz`i', hazard zeros
	local graphhaz = "`graphhaz' haz`i'"
	predict surv`i', survival zeros
	local graphsurv = "`graphsurv' surv`i'"
}

line `graphhaz' _t, sort ///
legend(cols(4) label(1 "1 d.f.") label(2 "2 d.f.") label(3 "3 d.f.") ///
label(4 "4 d.f.") label(5 "5 d.f.") label(6 "6 d.f.") label(7 "7 d.f.") ///
label(8 "8 d.f.") label(9 "9 d.f.") label(10 "10 d.f.")) ///
title("Baseline hazard function with different numbers of knots") ///
ytitle("Predicted mortality rate") xtitle("Time from diagnosis, years")
graph export "Writeup\Graphs\Myeloid\HazKnots.png", replace
	
line `graphsurv' _t, sort ///
legend(cols(4) label(1 "1 d.f.") label(2 "2 d.f.") label(3 "3 d.f.") ///
label(4 "4 d.f.") label(5 "5 d.f.") label(6 "6 d.f.") label(7 "7 d.f.") ///
label(8 "8 d.f.") label(9 "9 d.f.") label(10 "10 d.f.")) ///
title("Baseline survival function with different numbers of knots") ///
ytitle("Proportion alive") xtitle("Time from diagnosis, years")
graph export "Writeup\Graphs\Myeloid\SurvKnots.png", replace
	
	
est stats m*
esttab using "Writeup\Tables\fpm.tex", label nostar replace ///
title(Different numbers of knots\label{tab:Knots})


range tt 0 6 1001

drop hr
drop hrtd*

stpm2 trtf, scale(h) df(4) failconvlininit
predict hr, hrnum(trtf 2) hrdenom(trtf 1) timevar(tt)
summarize hr
local refline = r(mean)

stpm2 trtf, scale(h) df(4) tvc(trtf) dftvc(3) failconvlininit
predict hrtd, hrnum(trtf 2) hrdenom(trtf 1) timevar(tt) ci

graph twoway (rarea hrtd_lci hrtd_uci tt, color(gs14)) ///
	(line hrtd tt) ///
	, xtitle("Time since diagnosis, years") ///
	ytitle("Hazard ratio") name(hrtde, replace) ///
	legend(label(1 "Hazard ratio") label(2 "95% Confidence Interval") rows(2))

graph twoway (line hrtd tt) ///
	(line hr tt, lpattern(dash)) ///
	, xtitle("Time since diagnosis, years") ///
	ytitle("Hazard ratio") name(hr_ref,replace) ///
	legend(label(1 "Time-dependent effects") label(2 "Proportional hazards") rows(2))

graph combine hrtde hr_ref, title("Estimated hazard ratio")
graph export "Writeup\Graphs\Myeloid\fpm_tde.png", replace

	
* Gen event indicators
gen tx = txtime != .
gen cr = crtime != .
gen rl = rltime != .

msset, id(id) states(tx cr rl death) ///
	times(txtime crtime rltime futime)

mat tmat = r(transmatrix)
matrix list tmat

stset _stop, enter(_start) failure(_status=1)

msaj, id(id) transmat(tmat) ci

