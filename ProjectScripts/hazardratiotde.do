capture cd "C:\Users\infin\Documents\Leicester\Project"
local postdir = "Stata\Simulations\Sim4\outputs\"

local ntrans = 3
local flexdf = "3 5 8"
local tdedf = "1 3 5"
local months = "3 6 12 24 48"
local models = "rp3_td1 rp5_td1 rp5_td3 rp8_td1 rp8_td3 rp8_td5"
local at = 1

local collist = ""
foreach model of local models {
	forvalues tr=1/`ntrans' {
		foreach month of local months { 
					local collist = "`collist' `model'_`month'_at`at'_`tr' `model'_`month'_at`at'_`tr'_lci `model'_`month'_at`at'_`tr'_uci"
		}
	}
}
di "`collist'"

tempname hazrat
//postfile `hazrat' run df tdf tr months hazrat hrlci hruci using `postdir'hazrat_tde, replace
postfile `hazrat' `collist' using `postdir'hazrat_tde_wide, replace
set trace off
forvalues i = 1/100 {
	display "Run `i'"
	use "Stata\Simulations\Sim4\test`i'", clear
	range tt 0 5 1001
	local postpars = ""
	foreach df of local flexdf {
		foreach tdf of local tdedf {
			if `tdf' < `df' {
				forvalues tr = 1/`ntrans' {
					capture qui stpm2 trt if _trans==`tr', scale(h) df(`df') ///
						tvc(trt) dftvc(`tdf') iter(100) failconvlininit
					if (_rc != 0 | e(converged) != 1) {
						di "m`tr'_rp`df' did not converge"
						gen rp`df'_t`tdf'_hr`tr' = .
						gen rp`df'_t`tdf'_hr`tr'_lci = .
						gen rp`df'_t`tdf'_hr`tr'_uci = .
					} 
					else {
						predict rp`df'_t`tdf'_hr`tr', hrnum(trt 1) hrdenom(trt 0) ci timevar(tt)
						}
					foreach month of local months{
						qui summ rp`df'_t`tdf'_hr`tr' if tt==`month'/12
						local hr = r(mean)
						qui summ rp`df'_t`tdf'_hr`tr'_lci if tt==`month'/12
						local hrl = r(mean)
						qui summ rp`df'_t`tdf'_hr`tr'_uci if tt==`month'/12
						local hru = r(mean)
						
						local postpars = "`postpars' (`hr') (`hrl') (`hru')"
						//post `hazrat' (`i') (`df') (`tdf') (`tr') (`month') (`hr') (`hrl') (`hru')
						
					}
				}
			}
		}
	}
	post `hazrat' `postpars'
}
postclose `hazrat'