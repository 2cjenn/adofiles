* STATA DO FILE
* Project
* Multistate course - Acute myeloid leukaemia data
* 04/06/2019

* set Stata version
version 15.1

* start logging
capture log using "log.log", append

* set working directory
capture cd "C:\Users\infin\Documents\Leicester\Project"

********************************************************************************
* 
*-------------------------------------------------------------------------------
* Load in the data
*-------------------------------------------------------------------------------
use "Data\myeloid", clear

* Make treatment a factor
encode trt, gen(trtf)
levels(trtf)

* Gen event indicators
gen remission = crtime != .
gen relapse = rltime != .

tab death

*-------------------------------------------------------------------------------
* Investigate the simple cancer->remission-> relapse->death model
*-------------------------------------------------------------------------------

* Status column: 0 "Cancer" 1 "Remission" 2 "Relapsed" 3 "Dead"
capture drop stattemp
gen stattemp = crtime != .
replace stattemp = 2 if rltime > crtime & rltime != .
replace stattemp = 3 if death

label define statlbl 0 "Diagnosis" 1 "Remission" 2 "Relapse" 3 "Death"
label values stattemp statlbl
capture drop status
egen status = group(stattemp), label
tab status
drop stattemp

* prepare transition matrix
mat tmat = (.,1,.,2\.,.,3,4\.,.,.,5\.,.,.,.)
mat list tmat

* prepare data
msset, id(id) transmat(tmat) states(remission relapse death) times(crtime rltime futime) covariates(trtf)

* Count how many patients underwent each transition
matrix freqmat = r(freqmatrix)
mat list freqmat
local rdim = rowsof(freqmat)
local cdim = colsof(freqmat) - 1

capture file close latrix
file open latrix using "Writeup\Tables\latrix3.tex", write replace
// header
file write latrix "$$\begin{pmatrix}" _newline 
forvalues i = 1/`rdim' {
	forvalues j = 1/`cdim' {
		local val = freqmat[`i',`j']
		file write latrix "`val' & " 
	}
	local lastval = freqmat[`i', `cdim'+1]
	file write latrix "`lastval' \\" _newline
}
file write latrix "\end{pmatrix}$$"
file close latrix

* Declare survival data
stset _stop, enter(_start) failure(_status=1) scale(365.25)

* Create visual summary
msboxes, transmat(tmat) id(id)  xvalues(0.2 0.5 0.8 0.5) yvalues(0.7 0.7 0.7 0.2) ///
			boxheight(0.15) boxwidth(0.15) statenames("Diagnosis" "Remission" "Relapse" "Death")
graph export "Writeup\Graphs\Myeloid\QuadFlow.png", replace

// Calculate and plot the non-parametric Aalen-Johansen estimates of transition probabilities
* By treatment
msaj, transmat(tmat) id(id) by(trtf)
rename P_AJ_* P_trt_AJ_*

levelsof status, local(levels) 
foreach i of local levels {
	local val`i' : label status `i'
	display "`val`i''"
	twoway (line P_trt_AJ_`i' _t if trtf==1, sort connect(stepstair)) ///
			(line P_trt_AJ_`i' _t if trtf==2, sort connect(stepstair)) , 		///
			title("`val`i''") nodraw		///
			legend(order(1 "Treatment A" 2 "Treatment B"))	///
			xtitle("Time since diagnosis, years") ytitle("Probability ") ///
			ylab(0.0(0.2)1.0) xlab(0(1)7) name(TrtAJ`i', replace)
}
grc1leg2 TrtAJ1 TrtAJ2 TrtAJ3 TrtAJ4, legendfrom(TrtAJ1) ycommon ///
title("Aalen-Johansen estimates of" "transition probabilities by treatment")
graph export "Writeup\Graphs\Myeloid\AJ_Trt.png", replace

* Overall
msaj, transmat(tmat) id(id)

twoway (line P_AJ* _t, sort connect(stepstair)) , 		///
		title("Aalen-Johansen estimates of transition probabilities")		///
		legend(order(1 "Post-diagnosis" 2 "Remission" 3 "Relapse" 4 "Death"))	///
		xtitle("Time since diagnosis, years") ytitle("Transition probability ") ///
		ylab(0.0(0.2)1.0) xlab(0(1)7) name(AJ, replace)
graph export "Writeup\Graphs\Myeloid\AJ.png", replace

gen nothing = 0
replace nothing = . if P_AJ_1 == .
gen postdiag = P_AJ_1
gen rem = postdiag + P_AJ_2
gen rel = rem + P_AJ_3
gen deat = rel + P_AJ_4

twoway (rarea nothing postdiag _t, sort fintensity(100)) ///
	   (rarea postdiag rem _t, sort fintensity(100)) ///
	   (rarea rem rel _t, sort fintensity(100)) ///
	   (rarea rel deat _t, sort fintensity(100)), ///
	   legend(label(1 "Post-diagnosis") label(2 "Remission") label(3 "Relapse") label(4 "Death")) ///
	   ytitle("Probability") xtitle("Time since diagnosis, years") ///
	   title("Stacked Aalen-Johansen transition probability estimates") ///
		xlab(0(1)7) name(stackAJ, replace)
graph export "Writeup\Graphs\Myeloid\AJstack.png", replace



// Fit Weibull model for each transition 
forvalues i = 1/5 { 
	display "Transition `i'" 
	streg if _trans == `i', dist(exp) nolog noshow 
	estimates store m_wei`i'
}

// Fit mixture Weibull model for each transition 
forvalues i = 1/5 { 
	display "Transition `i'" 
	streg if _trans == `i', dist(exp) nolog noshow 
	estimates store m_wei`i'
}


// Fit stpm2 model for each transition 
// Choose best model
capture est drop m*
forvalues i=1/5 {
	forvalues df=1/9 {
		capture qui stpm2 trtf if _trans==`i', scale(h) df(`df') failconvlininit
		if _rc != 0 {
			di "m`i'_`df' did not converge"
		} 
		else {
			est store m`i'_`df'
		}
	}
	qui count if _trans==`i'& _d==1
	est stats m`i'_*, n(`r(N)')
}

* final models
stpm2 if _trans==1, scale(h) df(3) failconvlininit
est store m_rp1
stpm2 if _trans==2, scale(h) df(4) failconvlininit
est store m_rp2
stpm2 if _trans==3, scale(h) df(2) failconvlininit
est store m_rp3
stpm2 if _trans==4, scale(h) df(2) failconvlininit
est store m_rp4
stpm2 if _trans==5, scale(h) df(2) failconvlininit
est store m_rp5


// predictions for Weibull and flex par models
capture drop tt
range tt 0 6 1000
capture range tt50 0 6 50

predictms, transmatrix(tmat) /// 
	models(m_wei1 m_wei2 m_wei3 m_wei4 m_wei5) ///
	n(1000000) timevar(tt)
rename _prob_at1_1_* p_wei_1000_*

timer on 1
predictms, transmatrix(tmat) /// 
	models(m_rp1 m_rp2 m_rp3 m_rp4 m_rp5) ///
	n(1000000) timevar(tt)
rename _prob_at1_1_* p_rp_1000_*
timer off 1
timer list 1
// Use AJ option
timer on 2
predictms, transmatrix(tmat) /// 
	models(m_rp1 m_rp2 m_rp3 m_rp4 m_rp5) ///
	timevar(tt) aj
rename _prob_at1_1_* p_rp_aj1000_*
timer off 2
timer list 2

capture range tt100 0 6 100
predictms, transmatrix(tmat) /// 
	models(m_rp1 m_rp2 m_rp3 m_rp4 m_rp5) ///
	timevar(tt100) aj
rename _prob_at1_1_* p_rp_aj100_*

capture range tt10000 0 6 10000
predictms, transmatrix(tmat) /// 
	models(m_rp1 m_rp2 m_rp3 m_rp4 m_rp5) ///
	timevar(tt10000) aj
rename _prob_at1_1_* p_rp_aj10000_*
/*
predictms, transmat(tmat) models(m_rp1 m_rp2 m_rp3 m_rp4 m_rp5) obs(500) ///
graph graphopts(xlabel(0(1)7) ///
legend(label(1 "Post-diagnosis") label(2 "Remission") label(3 "Relapse") label(4 "Death")))
graph export "Writeup\Graphs\Myeloid\FlexStackProb.png", replace
*/

* Plot them all together
local statelist Diagnosis Remission Relapse Death
local n : word count `agrp'
forvalues i = 1/4 {
	twoway (line P_AJ_`i' _t, sort) ///
	(line p_wei_1000_`i' tt, sort) ///
	(line p_rp_1000_`i' tt, sort) ///
	, name(g`i', replace) title("`: word `i' of `statelist''") ///
	ytitle("Transition Probability") xtitle("Time since diagnosis, years") ylab(0.0(0.2)1.0) xlab(0(1)6) nodraw ///
	legend(label(1 "Aalen-Johansen") label(2 "Weibull") label(3 "Flexible parametric, 3 d.f."))
}

grc1leg2 g1 g2 g3 g4, legendfrom(g1) ycommon ///
title("Comparing Aalen-Johansen, Weibull" "and flexible parametric transition probabilities")
graph export "Writeup\Graphs\Myeloid\WeibFlexAJ.png", replace


// Compare simulation to AJ results
local statelist Diagnosis Remission Relapse Death
local n : word count `agrp'
forvalues i = 1/4 {
	twoway (scatter p_rp_aj1000_`i' p_rp_1000_`i', msize(tiny)) ///
	(function y=x, lpattern(dash)) ///
	, name(g`i', replace) title("`: word `i' of `statelist''") ///
	ytitle("AJ hybrid") xtitle("Simulation") ///
	nodraw legend(label(1 "Transition probability") label(2 "Reference line y=x"))
}
grc1leg2 g1 g2 g3 g4, ycommon ///
title("Comparing transition probabilities estimated using""simulation and hybrid Aalen-Johansen approaches")
graph export "Writeup\Graphs\Myeloid\SimvsAJhybrid.png", replace

gen test4 = p_rp_aj1000_4 - p_rp_1000_4
hist test4

scatter p_rp_aj1000_1 p_rp_1000_1

// Compare AJ results with different numbers of timepoints
local statelist Diagnosis Remission Relapse Death
local n : word count `agrp'
forvalues i = 1/4 {
	twoway (line p_rp_aj100_`i' tt100, sort) ///
	(line p_rp_aj1000_`i' tt, sort) ///
	(line p_rp_aj10000_`i' tt1000, sort) ///
	, name(g`i', replace) title("`: word `i' of `statelist''") ///
	ytitle("Transition Probability") xtitle("Time since diagnosis, years") ///
	ylab(0.0(0.2)1.0) xlab(0(1)6) nodraw ///
	legend(label(1 "100 time points") label(2 "1000 time points") label(3 "10,000 time points"))
}
grc1leg2 g1 g2 g3 g4, ycommon ///
title("Comparing transition probabilities estimated using""hybrid Aalen-Johansen approach")
graph export "Writeup\Graphs\Myeloid\AJtimepoints.png", replace

// Predict the Nelson-Aalen estimate of the cumulative hazard function
// Compare to estimate from models for each transition
capture drop na
sts gen na = na, by(_trans)

cap drop ch*
est restore m_rp1
predict ch1, cumhazard 
est restore m_rp2
predict ch2, cumhazard  
est restore m_rp3
predict ch3, cumhazard 
est restore m_rp4
predict ch4, cumhazard
est restore m_rp5
predict ch5, cumhazard

twoway 	(line ch1 na _t if _trans==1,sort lcol(red red) connect(direct stepstair) lpat(dash))	///
		(line ch2 na _t if _trans==2,sort lcol(blue blue) connect(direct stepstair) lpat(dash))	///
		(line ch3 na _t if _trans==3,sort lcol(green green) connect(direct stepstair) lpat(dash))	///
		(line ch4 na _t if _trans==4,sort lcol(orange orange) connect(direct stepstair) lpat(dash))	///
		(line ch5 na _t if _trans==5,sort lcol(purple purple) connect(direct stepstair) lpat(dash))	///
		, name(g1,replace) title("Best fitting flexible parametric cumulative hazard curve" ///
		"for each transition, overlaid on the Nelson-Aalen estimates") ///
		xtitle("Time since diagnosis, years") ytitle("Cumulative hazard") ///
		legend(order(1 "1. Diagnosis -> Remission" 3 "2. Diagnosis -> Death" 5 "3. Remission -> Relapse" ///
		7 "4. Remission -> Death" 9 "5. Relapse -> Death") cols(1))
graph export "Writeup\Graphs\Myeloid\CumuHaz.png", replace


// Consider length of stay
predictms , transmat(tmat) models(m_rp1 m_rp2 m_rp3 m_rp4 m_rp5) timevar(tt)	///
			at1(trtf 1) 			///
			at2(trtf 2)		///
			los ci	///
			seed(74314)					///
			m(500) aj

local statelist Diagnosis Remission Relapse Death
local n : word count `agrp'
forvalues i=1/4 {
	twoway (rarea _los_at2_1_`i'_lci _los_at2_1_`i'_uci tt) ///
		(line _los_at2_1_`i' tt)	///
		, title("`: word `i' of `statelist''") ///
		legend(label(1 "95% confidence interval") label(2 "Length of stay")) ///
		xtitle("Time since diagnosis, years")  ytitle("Length of stay, years") ///
		ylabel(0(1)4) name(los`i', replace) nodraw
}
grc1leg2 los1 los2 los3 los4, legendfrom(los1) ycommon ///
title("Length of stay in each state")
graph export "Writeup\Graphs\Myeloid\LoS.png", replace

* Plot them all on one area graph
capture gen bottom = 0
capture gen first = _los_at2_1_1
capture gen second  = _los_at2_1_2 + first
capture gen third = _los_at2_1_3 + second
capture gen top = _los_at2_1_4 + third

graph twoway (rarea first bottom tt) ///
	(rarea second first tt) ///
	(rarea third second tt) ///
	(rarea top third tt), ///
	title("Length of stay in each state") xlabel(0(1)6) ///
	xtitle("Time since diagnosis, years")  ytitle("Length of stay, years") ///
	legend(label(1 "Diagnosis") label(2 "Remission") label(3 "Relapse") label(4 "Death")) ///
	name(loscombine, replace)
graph export "Writeup\Graphs\Myeloid\LoScombine.png", replace
	
capture gen diff = top - ttimevar
line diff ttimevar
* why does it jump in that first time point?



// Investigate non-proportional hazards
stpm2 trtf if _trans==1, scale(h) df(3) failconvlininit
est store m1
predict hrtd1, hrnum(trtf 2) hrdenom(trtf 1) timevar(tt) ci
stpm2 trtf if _trans==2, scale(h) df(3) tvc(trtf) dftvc(2) failconvlininit
est store m2
predict hrtd2, hrnum(trtf 2) hrdenom(trtf 1) timevar(tt) ci
stpm2 trtf if _trans==3, scale(h) df(3) failconvlininit
est store m3
predict hrtd3, hrnum(trtf 2) hrdenom(trtf 1) timevar(tt) ci
stpm2 trtf if _trans==4, scale(h) df(3) tvc(trtf) dftvc(2) failconvlininit
est store m4
predict hrtd4, hrnum(trtf 2) hrdenom(trtf 1) timevar(tt) ci
stpm2 trtf if _trans==5, scale(h) df(3) tvc(trtf) dftvc(2) failconvlininit
est store m5
predict hrtd5, hrnum(trtf 2) hrdenom(trtf 1) timevar(tt) ci

predictms , transmat(tmat) models(m1 m2 m3 m4 m5) timevar(tt)	///
			at1(trtf 1) at2(trtf 2) difference seed(7434) aj

* Plot them all
forvalues tr = 1/5 {
	graph twoway (line hrtd`tr' tt), ///
		title("Transition `tr'") xlabel(0(1)6) ///
		xtitle("Time since diagnosis, years")  ytitle("Probability") ///
		legend(off) name(timedepprob`tr', replace)
}
graph combine timedepprob2 timedepprob4 timedepprob5, ///
	ycommon title("Hazard ratios for each transition")
graph export "Writeup\Graphs\Myeloid\TimeDepHR.png", replace

graph twoway (line _diff_prob_at2_1_1 tt) ///
	(line _diff_prob_at2_1_2 tt) ///
	(line _diff_prob_at2_1_3 tt) ///
	(line _diff_prob_at2_1_4 tt) ///
	, title("Difference in transition probabilities between""Treatment A and Treatment B") ///
	ytitle("Difference in probability") xtitle("Time since diagnosis, years") ///
	legend(label(1 "Diagnosis") label(2 "Remission") label(3 "Relapse") label(4 "Death"))
graph export "Writeup\Graphs\Myeloid\TimeDepProbDiff.png", replace

* The only ones that run say non-sig so...why?
forvalues i=1/5{
	sts graph if _trans==`i', risktable by(trtf) name(trans`i', replace) ///
	title("Transition `i'") xtitle("Time since diagnosis, years")
	graph export "Writeup\Graphs\Myeloid\Trans`i'.png", replace
}
grc1leg2 trans1 trans2 trans3 trans4 trans5, legendfrom(trans1) ycommon ///
title("Something")
graph export "Writeup\Graphs\Myeloid\TransTrt.png", replace

// Investigate impact of treatment
capture drop ttimevar
range ttimevar 0 7 1000
predictms , transmat(tmat) models(m1 m2 m3 m4 m5) timevar(ttimevar)	///
			at1(trtf 1) 			///
			at2(trtf 2)		///
			difference ratio ci		///
			seed(74314)					///
			m(500) aj

local statelist Diagnosis Remission Relapse Death
local n : word count `agrp'
forvalues i=1/4 {
	twoway (rarea _ratio_prob_at2_1_`i'_lci _ratio_prob_at2_1_`i'_uci ttimevar) ///
		(line _ratio_prob_at2_1_`i' ttimevar)	///
		, title("`: word `i' of `statelist''") ///
		legend(label(1 "95% confidence interval") label(2 "Ratio of probabilities")) ///
		xtitle("Time since diagnosis, years")  ytitle("Ratio") ///
		ylabel(0(0.5)2) name(ratio`i', replace) nodraw
}
grc1leg2 ratio1 ratio2 ratio3 ratio4, legendfrom(ratio1) ycommon ///
title("Ratio of transition probabilities, Treatment A : Treatment B")
graph export "Writeup\Graphs\Myeloid\RatioAB.png", replace




// Clock reset
* Is Markov assumption violated?
gen cryr = crtime/365.25
stpm2 cryr if _trans==3, scale(hazard) df(3) failconvlininit

* Yes, so try clock-reset approach
gen _newt = _stop - _start
stset _newt, failure(_status=1) scale(365.25)

// Fit stpm2 model with 3 df for each transition 
forvalues i = 1/5 {
	display "Transition `i'" 
	stpm2 trtf if _trans==`i', scale(hazard) df(3) failconvlininit
	estimates store m_clockres`i'
	*predict hr_f`i', hr
}
range tt 0 6 1000
predictms, transmatrix(tmat) /// 
	models(m_clockres1 m_clockres2 m_clockres3 m_clockres4 m_clockres5) ///
	reset at1(trtf 1) timevar(tt)
rename _prob_at1_1_* p_clockres_500_*


* Plot them all together
local statelist Diagnosis Remission Relapse Death
local n : word count `agrp'
forvalues i = 1/4 {
	twoway (line P_AJ_`i' _t, sort) ///
	(line p_clockres_500_`i' tt, sort) ///
	(line p_rp_500_`i' tt, sort) ///
	, name(g`i', replace) title("`: word `i' of `statelist''") ///
	ytitle("Transition Probability") xtitle("Time since diagnosis, years") ylab(0.0(0.2)1.0) xlab(0(1)7) nodraw ///
	legend(label(1 "Aalen-Johansen") label(2 "Clock-reset") label(3 "Flexible parametric"))
}
grc1leg2 g1 g2 g3 g4, legendfrom(g1) ycommon ///
title("Comparing Aalen-Johansen, clock-reset" "and flexible parametric transition probabilities")
graph export "Writeup\Graphs\Myeloid\ClockFlexAJ.png", replace

