********************************************************************************
* Scenario 1
********************************************************************************
* Motivation: Simple Case
* Model each transition with a basic Weibull (p=1)
capture cd "C:\Users\infin\Documents\Leicester\Project"
* Assign lambda and gamma values:
* Healthy -> Illness
local l11 = 1.3
local g11 = 0.6
local l12 = 0
local g12 = 0
local p1 = 1

* Illness -> Death
local l21 = 0.4
local g21 = 0.8
local l22 = 0
local g22 = 0
local p2 = 1

* Healthy -> Death
local l31 = 0.2
local g31 = 0.6
local l32 = 0
local g32 = 0
local p3 = 1
/*
visualise, seed(398894) ntrans(3) pmix(`p1' `p2' `p3') maxtime(5) ///
	gammas1(`g11' `g21' `g31') lambdas1(`l11' `l21' `l31') ///
	gammas2(`g12' `g22' `g32') lambdas2(`l12' `l22' `l32') ///
	outdir("Writeup\Graphs\Sim1\") simulation(1)

capture program drop illmixweib
illmixweib, obs(10000000) seed(398894) ntrans(3) pmix(`p1' `p2' `p3') maxtime(5) ///
	gammas1(`g11' `g21' `g31') lambdas1(`l11' `l21' `l31') ///
	gammas2(`g12' `g22' `g32') lambdas2(`l12' `l22' `l32') ///	
	truevals(3 6 12 24 48) postdir("Stata\Simulations\Sim1\outputs\")

simdata, outfile("Stata\Simulations\Sim1\test") nruns(1000) obs(1000) ///
	seed(398894) ntrans(3) pmix(`p1' `p2' `p3') maxtime(5) ///
	gammas1(`g11' `g21' `g31') lambdas1(`l11' `l21' `l31') ///
	gammas2(`g12' `g22' `g32') lambdas2(`l12' `l22' `l32')
	
//set trace on
capture program drop fitdata
fitdata, infile("Stata\Simulations\Sim1\test") postdir("Stata\Simulations\Sim1\outputs\") ///
	  ci nruns(1000) ntrans(3) maxtime(5) months(3 6 12 24 48) flexdf(1 2 3 4 5 6 7 8)
*/	 

capture program drop fitdata_logit
fitdata_logit, infile("Stata\Simulations\Sim1\test") postdir("Stata\Simulations\Sim1\outputs\") ///
	  ci nruns(1) ntrans(3) maxtime(5) months(3 6 12 24 48) flexdf(1)

//use "Stata\Simulations\Sim1\outputs\transprob", clear