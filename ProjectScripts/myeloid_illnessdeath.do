* STATA DO FILE
* Project
* Multistate course - Acute myeloid leukaemia data
* 04/06/2019

* set Stata version
version 15.1

* start logging
capture log using "log.log", append

* set working directory
capture cd "C:\Users\infin\Documents\Leicester\Project"

********************************************************************************
* 
*-------------------------------------------------------------------------------
* Load in the data
*-------------------------------------------------------------------------------
use "Data\myeloid", clear

* Make treatment a factor
encode trt, gen(trtf)
levels(trtf)

* Gen event indicators
gen remission = crtime != .

tab death

*-------------------------------------------------------------------------------
* Investigate the simple cancer->remission-> relapse->death model
*-------------------------------------------------------------------------------

* Status column: 0 "Diagnosis" 1 "Relapsed" 2 "Dead"
capture drop stattemp
gen stattemp = crtime != .
replace stattemp = 2 if death

label define statlbl 0 "Diagnosis" 1 "Remission" 2 "Death"
label values stattemp statlbl
capture drop status
egen status = group(stattemp), label
tab status
drop stattemp

* prepare transition matrix
mat tmat = (.,1,2\.,.,3\.,.,.)
mat list tmat

* prepare data
msset, id(id) transmat(tmat) states(remission death) times(crtime futime) covariates(trtf)

* Count how many patients underwent each transition
matrix freqmat = r(freqmatrix)
mat list freqmat
local rdim = rowsof(freqmat)
local cdim = colsof(freqmat) - 1

* Declare survival data
stset _stop, enter(_start) failure(_status=1) scale(365.25)

* Create visual summary
msboxes, transmat(tmat) id(id)  xvalues(0.2 0.8 0.5) yvalues(0.7 0.7 0.2) ///
			boxheight(0.15) boxwidth(0.15) statenames("Diagnosis" "Remission" "Death")
graph export "Writeup\Graphs\Myeloid\TriFlow.png", replace

// Calculate and plot the non-parametric Aalen-Johansen estimates of transition probabilities
* By treatment
msaj, transmat(tmat) id(id) by(trtf)
rename P_AJ_* P_trt_AJ_*

levelsof status, local(levels) 
foreach i of local levels {
	local val`i' : label status `i'
	display "`val`i''"
	twoway (line P_trt_AJ_`i' _t if trtf==1, sort connect(stepstair)) ///
			(line P_trt_AJ_`i' _t if trtf==2, sort connect(stepstair)) , 		///
			title("`val`i''") nodraw		///
			legend(order(1 "Treatment A" 2 "Treatment B"))	///
			xtitle("Follow-up time (years)") ytitle("Probability ") ///
			ylab(0.0(0.2)1.0) xlab(0(1)7) name(TrtAJ`i', replace)
}
grc1leg2 TrtAJ1 TrtAJ2 TrtAJ3, legendfrom(TrtAJ1) ycommon ///
title("Aalen-Johansen estimates of" "transition probabilities by treatment")
//graph export "Writeup\Graphs\Myeloid\AJ_Trt.png", replace

* Overall
msaj, transmat(tmat) id(id)

twoway (line P_AJ* _t, sort connect(stepstair)) , 		///
		title("Aalen-Johansen estimates of transition probabilities")		///
		legend(order(1 "Post diagnosis" 2 "Relapse" 3 "Death"))	///
		xtitle("Follow-up time (years)") ytitle("Transition probability ") ///
		ylab(0.0(0.2)1.0) xlab(0(1)7) name(AJ, replace)
//graph export "Writeup\Graphs\Myeloid\AJ.png", replace
		


// Fit Weibull model for each transition 
forvalues i = 1/3 { 
	display "Transition `i'" 
	streg if _trans == `i', dist(weib) 
	estimates store m_wei`i'
	local lambda`i' = exp(_b[_cons])
	local gamma`i' = exp(_b[/ln_p])
	display `lambda`i'' 
	display `gamma`i''
	stcurve, hazard name(haz`i', replace)
}


// Fit mixture Weibull model for each transition 
forvalues i = 1/3 { 
	display "Transition `i'" 
	stmix if _trans == `i', dist(weibweib) iter(5)
	estimates store m_mixwei`i'
}