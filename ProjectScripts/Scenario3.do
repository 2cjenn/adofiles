********************************************************************************
* Scenario 3
********************************************************************************
* Incorporate binary covariate
capture cd "C:\Users\infin\Documents\Leicester\Project"
* Assign lambda and gamma values:
* Healthy -> Illness
local l11 = 0.2
local g11 = 0.8
local l12 = 1.6
local g12 = 1.0
local p1 = 0.2
local beta1 = 0.4

* Illness -> Death
local l21 = 1.0
local g21 = 1.5
local l22 = 1.0
local g22 = 0.5
local p2 = 0.5
local beta2 = 0.8

* Healthy -> Death
local l31 = 0.03
local g31 = 1.9
local l32 = 0.3
local g32 = 2.5
local p3 = 0.7
local beta3 = 0.6
/*
capture program drop visualise
visualise, seed(398894) ntrans(3) pmix(`p1' `p2' `p3') maxtime(5) ///
	gammas1(`g11' `g21' `g31') lambdas1(`l11' `l21' `l31') ///
	gammas2(`g12' `g22' `g32') lambdas2(`l12' `l22' `l32') ///
 	outdir("Writeup\Graphs\Sim3\") simulation(3) loghr(`beta1' `beta2' `beta3')

simdata, outfile("Stata\Simulations\Sim3\test") nruns(1) obs(1000) ///
	seed(398894) ntrans(3) pmix(`p1' `p2' `p3') maxtime(5) ///
	gammas1(`g11' `g21' `g31') lambdas1(`l11' `l21' `l31') ///
	gammas2(`g12' `g22' `g32') lambdas2(`l12' `l22' `l32') ///
	covar1(0.5 trt `beta1' `beta2' `beta3')
*/
capture cd "C:\Users\infin\Documents\Leicester\Project"
capture program drop illmixweib
illmixweib, obs(1000000) seed(398894) ntrans(3) pmix(`p1' `p2' `p3') maxtime(5) ///
	gammas1(`g11' `g21' `g31') lambdas1(`l11' `l21' `l31') ///
	gammas2(`g12' `g22' `g32') lambdas2(`l12' `l22' `l32') ///
	covar1(0.5 trt `beta1' `beta2' `beta3') difference ///
	truevals(3 6 12 24 48) postdir("Stata\Simulations\Sim3\outputs\")

capture program drop fitdata
fitdata, infile("Stata\Simulations\Sim3\test") postdir("Stata\Simulations\Sim3\outputs\")  ///
	nruns(1000) ntrans(3) maxtime(5) months(3 6 12 24 48) flexdf(1 2 3 4 5 6 7 8) ///
	covariates(trt) at1(trt 0) at2(trt 1) difference ci


forvalues tr=1/3 {
	line diff`tr'_rp1 diff`tr'_rp2 diff`tr'_rp3 diff`tr'_rp4 diff`tr'_rp5 diff`tr'_rp6 diff`tr'_rp7 diff`tr'_rp8 tt, ///
	title("Transition `tr'") name(diff`tr', replace)
	
	graph twoway (line h`tr'_rp1_plac h`tr'_rp2_plac h`tr'_rp3_plac h`tr'_rp4_plac h`tr'_rp5_plac h`tr'_rp6_plac h`tr'_rp7_plac h`tr'_rp8_plac tt) ///
	(line h`tr'_rp1_trt h`tr'_rp2_trt h`tr'_rp3_trt h`tr'_rp4_trt h`tr'_rp5_trt h`tr'_rp6_trt h`tr'_rp7_trt h`tr'_rp8_trt tt, lpattern(dash dash dash dash dash dash dash dash)), ///
	legend(order(1 "Treatment = 0" 9 "Treatment = 1")) ///
	title("Transition `tr'") name(haz`tr', replace)
}
line diff1 diff2 diff3 tt

/*
capture program drop fitdata
fitdata, infile("Stata\Simulations\Sim3\test") postdir("Stata\Simulations\Sim3\outputs\")  ///
	nruns(100) ntrans(3) maxtime(5) months(3 6 12 24 48) flexdf(2 3 4 5 6 7 8) ///
	covariates(trt) at1(trt 0) at2(trt 1) diff

use Stata\Simulations\Sim3\outputs\diff_transprob, clear
	
forvalues tr=1/3 {
	graph twoway (rarea _diff_prob_at2_1_`tr'_lci _diff_prob_at2_1_`tr'_uci tt, color(gs12)) ///
		(line _diff_prob_at2_1_`tr' tt), ///
		title("Transition `tr'") xtitle("Time, years") ytitle("Transition probability") ///
		legend(label(1 "95% confidence interval") label(2 "Point estimate")) ///
		name(sim_`tr', replace) nodraw
}
grc1leg2 sim_1 sim_2 sim_3, ///
	legendfrom(sim_1) ycommon ///
	title("Differences in transition probabilities by treatment") ///
	subtitle("Scenario 3 run 33, flexible parametric models with 6 d.f.")
graph export "Writeup\Graphs\Sim3\confint_noaj.png", replace
*/

use "Stata\Simulations\Sim3\outputs\hazardratio.dta", clear
gen df = real(substr(model, -1, .))
drop model

reshape wide haz lci uci, i(run_no trans) j(df)
label variable haz1 "Weibull"
label variable haz2 "Flexible parametric, 2 d.f."
label variable haz3 "Flexible parametric, 3 d.f."
label variable haz5 "Flexible parametric, 5 d.f."
label variable haz8 "Flexible parametric, 8 d.f."

forvalues tr = 1/3 {
	graph matrix haz1 haz2 haz3 haz5 haz8 if trans==`tr', ms(p) half ///
		title("Scenario 3: Transition `tr'") ///
		subtitle("Scatter matrix of log hazard ratio of treatment covariate")
	graph export "Writeup\Graphs\Sim3\loghr_scatter`tr'.png", replace
}