* Opens the true values and simulated values, creates a pretty latex table
capture program drop tabres_tde
program define tabres_tde
	syntax [, 		///
		INDIR(string)					///
		POSTDIR(string)					///
		MONTHS(numlist)					///
		MODELS(string)					///
		NTRANS(int 1)					///
		ATVAL(int 1)					///
		DIFFerence						///
		SCENario(int 1)					/// -Scenario number-
		PLOT							///
		*								/// -Infinite covariates-
	]

	foreach label in "transprob_tde" "los_tde" {
		if "`label'" == "transprob_tde" {
			local caption = "transition probabilities"
			local truefile = "trueprob"
		} 
		else if "`label'" == "los_tde" {
			local caption = "length of stay"
			local truefile = "truelos"
		}
		else if "`label'" == "hazrat_tde_wide" {
			local caption = "hazard ratio"
			local truefile = "truehazrat"
		}
		if "`difference'" != "" {
			local label = "diff_`label'"
			local truefile = "`truefile'_trtdiff"
		}
		display "`label'"
		use `indir'diff_`label', clear
	
		capture frame create truth
		frame change truth
		use `indir'`truefile', clear

		foreach month of local months {
			global t1_`month' = healthy_`month'
			global t2_`month' = ill_`month'
			global t3_`month'= dead_`month'
		}
		local nmonths : word count `months'

		frame change default

		gen bias = .
		gen coverage = .
		gen squerror = .

		local cols = "lr"
		local colcount = 2
		foreach model of local models {
			local cols = "`cols'|rrr"
			local colcount = `colcount' + 3
			
			qui summarize `model'_3_at1_1
			local converged = r(N)
			
			qui summarize `model'_3_at1_1_lci
			local posprob = r(N)
			
			display "`model' converged `converged' times and has confidence intervals `posprob' times"
		}
		
		capture file close summary
		file open summary using "Writeup\Tables\Scenario`scenario'_`label'.tex", write replace
		// header
		file write summary	/// 
		"\begin{landscape}" _newline ///
		"\begin{table}[h]" _newline ///
		"\begin{center}" _newline
		if "`difference'" == "" {
			file write summary "\caption{Comparison of bias, 95\% coverage and mean squared errors for the estimated `caption' over 1000 replications of simulation Scenario `scenario'. Results are given at `nmonths' timepoints for each state.}" _newline
		}
		else {
			file write summary "\caption{Comparison of bias, 95\% coverage and mean squared errors for the estimated difference in `caption' between treatments over 1000 replications of simulation Scenario `scenario'. Results are given at `nmonths' timepoints for each state.}" _newline
		}
		file write summary"\vspace{5mm}" _newline ///
		"\label{tab:`label'Scenario`scenario'}" _newline ///
		"\begin{tabular}{`cols'}" _newline ///
		"\hline" _newline
		file write summary "\multirow{2}{*}{\bfseries Months} & \multirow{2}{*}{\bfseries Truth} "
		foreach model of local models {
			local df = substr("`model'", -(1 + strpos("`model'", "_")), 1)
			local tdf = substr("`model'", -1, .)
			local modelname = "FlexPar, `df', `tdf' df"

			file write summary "& \multicolumn{3}{c}{\bfseries `modelname'} "
		}
		file write summary " \\ \cmidrule(lr){3-`colcount'} " _newline
		file write summary "&"
		foreach model of local models {
			file write summary "& Bias & Cover & MSE "
		}
		file write summary " \\" _newline ///
		"\cmidrule(lr){1-`colcount'} " _newline
		
		tempname plotdf
		postfile `plotdf' trans time df tdf bias coverage mse using `postdir'plotdf_`label', replace
	
		forvalues tr = 1/`ntrans' {
			file write summary "\multicolumn{2}{l}{\bfseries State `tr'} \\" _newline ///
			"\cmidrule(lr){1-2} " _newline
			
			local graphtext = ""
			foreach month of local months {
				local truth = "t`tr'_`month'"
				local printtruth : display %4.3f $`truth'
				file write summary "`month' months & `printtruth'"
				foreach model of local models {
					quietly {
						if "`difference'" == "" {
							replace bias = `model'_`month'_at1_`tr' - $`truth'
							replace coverage = inrange($`truth', max(-1,`model'_`month'_at1_`tr'_lci), max(-1,`model'_`month'_at1_`tr'_uci))
							replace coverage = . if `model'_`month'_at1_`tr'_lci == .
						}
						else {
							replace bias = diff_`model'_`month'_`tr' - $`truth'
							replace coverage = inrange($`truth', max(-1,`model'_`month'_at1_`tr'_lci), max(-1,`model'_`month'_at1_`tr'_uci))
							replace coverage = . //if `model'_`month'_at1_`tr'_lci == .
						}
						qui summarize bias
						local meanbias = r(mean)
						local meanbias : display %4.3f `meanbias'
						
						qui summarize coverage
						local propcover = r(mean) * 100
						local propcover : display %4.1f `propcover'
						
						replace squerror = bias^2
						qui summarize squerror
						local mse = r(mean)
						local mse : display %4.3f `mse'
					}
					
					file write summary "& `meanbias' & `propcover' & `mse'"
					
					local df = substr("`model'", -(1 + strpos("`model'", "_")), 1)
					local tdf = substr("`model'", -1, .)
					local postpar = "(`tr') (`month') (`df') (`tdf') (`meanbias') (`propcover') (`mse')"
					post `plotdf' `postpar'
					
				}
				file write summary " \\" _newline
			}
			file write summary "\hline" _newline
		}
		
		file write summary ///
		"\end{tabular}" _newline ///
		"\end{center}" _newline ///
		"\end{table}" _newline ///
		"\end{landscape}"
		file close summary
		
		postclose `plotdf'
		
		capture frame drop truth
		if "`plot'" != "" {
			use "`postdir'plotdf_`label'", clear
			gen dftdf = df + tdf/10
			forvalues tr = 1/`ntrans' {
				sepscatter bias dftdf if trans==`tr', separate(time) yline(0) ///
				xtitle("Degrees of freedom") yscale(range(0)) title("Transition `tr'") ///
				legend(pos(1) rows(1) subtitle("Months")) name(biasdf_`label'_tr`tr', replace) nodraw
			}
			grc1leg2 biasdf_`label'_tr1 biasdf_`label'_tr2 biasdf_`label'_tr3, ///
			legendfrom(biasdf_`label'_tr1) ycommon cols(3) ///
			title("Scenario `scen': Mean bias in `caption' at each" "time point under time-dependent effects models")
			graph export "Writeup\Graphs\Sim`scenario'\biasdf_`label'.png", replace
		}
	}
	
end


capture cd "C:\Users\infin\Documents\Leicester\Project"
forvalues scen = 4/5 {		
	tabres_tde, indir("C:/Users/infin/Documents/Leicester/Project/Stata/Simulations/Sim`scen'/outputs/") ///
		postdir("Stata\Simulations\Sim`scen'\outputs\") ///
		scenario(`scen') ntrans(3) atval(1) tde plot ///
		months(3 6 12 24 48) models("rp3_td1 rp5_td1 rp5_td3 rp8_td3 rp8_td5")

	//capture program drop tabAIC
	//tabAIC, infile("Stata\Simulations\Sim`scen'\outputs\ABIC") scenario(`scen')
}
