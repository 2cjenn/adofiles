********************************************************************************
* Scenario 2
********************************************************************************
* Mixture Weibulls 
capture cd "C:\Users\infin\Documents\Leicester\Project"
* Assign lambda and gamma values:
* Healthy -> Illness
local l11 = 0.3
local g11 = 3.0
local l12 = 0.4
local g12 = 0.2
local p1 = 0.5

* Illness -> Death
local l21 = 0.2
local g21 = 1.6
local l22 = 0.1
local g22 = 1.0
local p2 = 0.2

* Healthy -> Death
local l31 = 0.3
local g31 = 0.6
local l32 = 1.3
local g32 = 1.0
local p3 = 0.5
/*
capture program drop visualise
visualise, seed(398894) ntrans(3) pmix(`p1' `p2' `p3') maxtime(5) ///
	gammas1(`g11' `g21' `g31') lambdas1(`l11' `l21' `l31') ///
	gammas2(`g12' `g22' `g32') lambdas2(`l12' `l22' `l32') ///
 	outdir("Writeup\Graphs\Sim2\") simulation(2)

capture program drop illmixweib
illmixweib, obs(10000000) seed(398894) ntrans(3) pmix(`p1' `p2' `p3') maxtime(5) ///
	gammas1(`g11' `g21' `g31') lambdas1(`l11' `l21' `l31') ///
	gammas2(`g12' `g22' `g32') lambdas2(`l12' `l22' `l32') ///
	truevals(3 6 12 24 48) postdir("Stata\Simulations\Sim2\outputs\")

	

simdata, outfile("Stata\Simulations\Sim2\test") nruns(1000) obs(1000) ///
	seed(398894) ntrans(3) pmix(`p1' `p2' `p3') maxtime(5) ///
	gammas1(`g11' `g21' `g31') lambdas1(`l11' `l21' `l31') ///
	gammas2(`g12' `g22' `g32') lambdas2(`l12' `l22' `l32') 
	///
	// covar1(0.5 trt 0.2 0.5 0.7) covar2(0.5 trtf 0.2 0.5 0.7)
*/
timer on 1
capture program drop fitdata
fitdata, infile("Stata\Simulations\Sim2\test") postdir("Stata\Simulations\Sim2\outputs\") ci ///
	nruns(1000) ntrans(3) maxtime(5) months(3 6 12 24 48) flexdf(1 2 3 4 5 6 7 8)
timer off 1
timer list 1
	///  23988.26 = 6.5hrs for CI with weibull and five flexpar models?
	covariates(trt) at1(trt 0) at2(trt 1)
	

*-------------------------------------------------------------------------------
* Look at one case where predictms gave negative probability error
* Confirm that this only happens when using the aj option

estimates use "Stata\Simulations\Sim2\outputs\Sim2_33"

predictms, transmatrix(transmatrix) timevar(tt) los ///
	models(m1_rp6 m2_rp6 m3_rp6) ci

forvalues tr=1/3 {
	graph twoway (rarea _prob_at1_1_`tr'_lci _prob_at1_1_`tr'_uci tt, color(gs12)) ///
		(line _prob_at1_1_`tr' tt), ///
		title("Transition `tr'") xtitle("Time, years") ytitle("Transition probability") ///
		legend(label(1 "95% confidence interval") label(2 "Point estimate")) ///
		name(sim33_`tr', replace) nodraw
}
grc1leg2 sim33_1 sim33_2 sim33_3, ///
	legendfrom(sim33_1) ycommon  ///
	title("Simulated transition probabilities and confidence intervals") ///
	subtitle("Scenario 2 run 33, flexible parametric models with 6 d.f.")
graph export "Writeup\Graphs\Sim2\confint_noaj.png", replace
*-------------------------------------------------------------------------------
* Fit models selected by AIC to the big run
stpm2 if _trans==1, scale(h) df(6) iter(100) failconvlininit
est store m1
stpm2 if _trans==2, scale(h) df(3) iter(100) failconvlininit
est store m2
stpm2 if _trans==3, scale(h) df(3) iter(100) failconvlininit
est store m3

capture drop tt
range tt 0 5 1001

predictms, transmatrix(transmatrix) timevar(tt) aj ///
	models(m1 m2 m3) graph
graph export "Writeup\Graphs\Sim2\AIC_choice.png", replace

predictms, transmatrix(transmatrix) timevar(tt) aj los ///
	models(m1 m2 m3) ci
					
*-------------------------------------------------------------------------------				
capture program drop tabAIC
tabAIC, outfile("Stata\Simulations\Sim2\outputs\ABIC")
* 7 x could not predict values
* 2 x flexpar could not converge

use "Stata\Simulations\Sim2\outputs\transprob", clear
sum rp3_3_2

graph matrix weib_3_at1_1 rp2_3_at1_1 rp3_3_at1_1 rp4_3_at1_1 rp5_3_at1_1 rp6_3_at1_1
scatter weib_3_at1_1 rp3_3_at1_1
