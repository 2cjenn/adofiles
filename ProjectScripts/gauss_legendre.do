* Healthy -> Illness
local l1 = 0.2
local g1 = 0.8
local l2 = 1.6
local g2 = 1.0
local p = 0.2
local maxtime = 5

clear
set obs 1000
set seed 245362
gen trt = runiform() < 0.5

local loghazard "ln((`p':*`l1':*`g1':*(#t:^(`g1':-1)):*exp(-`l1':*(#t:^`g1')) :+ (1:-`p'):*`l2':*`g2':*(#t:^(`g2':-1)):*exp(-`l2':*(#t:^`g2'))) :/ (`p':*exp(-`l1':*(#t:^`g1')) + (1-`p'):*exp(-`l2':*(#t:^`g2'))))"

survsimdelay dtime death, lambdas(`l1' `l2') gammas(`g1' `g2') ///
		distribution (weibull) maxtime(`maxtime') mixture pmix(`p')
survsimdelay time10 state10, loghazard(`loghazard') maxtime(`maxtime') nodes(10)
survsimdelay time100 state100, loghazard(`loghazard') maxtime(`maxtime') nodes(100)
survsimdelay time1000 state1000, loghazard(`loghazard') maxtime(`maxtime') nodes(1000)

stset dtime, failure(death)
sts gen surv = s
gen dt = _t

stset time10, failure(state10)
sts gen surv10 = s
gen t10 = _t

stset time100, failure(state100)
sts gen surv100 = s
gen t100 = _t

stset time1000, failure(state1000)
sts gen surv1000 = s
gen t1000 = _t

twoway (line surv dt, sort) ///
	(line surv10 t10, sort) ///
	(line surv100 t100, sort) ///
	(line surv1000 t1000, sort) ///
	, title("Kaplan-Meier curves for data simulated" "analytically and by Gauss-Legendre quadrature") ///
	ytitle("Survival proportion") xtitle("Follow up time, years") ///
	legend(label(1 "Analytic integration") label(2 "G-L, 10 nodes") label(3 "G-L, 100 nodes") label(4 "G-L, 1000 nodes"))
graph export "Writeup\Graphs\gausslegendre.png", replace