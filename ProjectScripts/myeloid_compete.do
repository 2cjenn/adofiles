* STATA DO FILE
* Project
* Multistate course - Acute myeloid leukaemia data
* 04/06/2019

* set Stata version
version 15.1

* start logging
capture log using "log.log", append

* set working directory
capture cd "C:\Users\infin\Documents\Leicester\Project"

********************************************************************************
* 
*-------------------------------------------------------------------------------
* Load in the data
*-------------------------------------------------------------------------------
use "Data\myeloid", clear

* Make treatment a factor
encode trt, gen(trtf)
levels(trtf)

*-------------------------------------------------------------------------------
* Competing risks
*-------------------------------------------------------------------------------
* Time to death or relapse
gen rl = rltime != .

* Status column: 0=healthy, 1=dead, 2=relapsed
gen status = death
replace status = 2 if rltime < futime
label define statlbl 0 "Healthy" 1 "Dead" 2 "Relapsed"
label values status statlbl
tab status

* Death
stset futime, failure(status==1) scale(365.25)
sts generate surv1_trt = s, by(trtf) 

* Relapse
stset futime, failure(status==2) scale(365.25)
sts generate surv2_trt = s, by(trtf) 

* Plot the complement of the Kaplan-Meier survival estimate for each cause
gen prob1_trt = 1-surv1_trt //Death
gen prob2_trt = 1-surv2_trt //Relapse

twoway (line prob1_trt _t if trtf == 1, sort lcolor(black) lpattern(solid)) ///
	   (line prob2_trt _t if trtf == 1, sort lcolor(black) lpattern(dash)), ///
	   ytitle("Probability of Death") ///
	   xtitle("Time since diagnosis, years") ///
	   legend(order(1 "Death" 2 "Relapse") pos(5)) ///
	   ylabel(0(0.1)0.5, angle(0) format(%3.1f)) name(KM, replace) 

// Use the stcompet command to estimate the cause specific CIF for both cancer and other causes. 
capture drop CIF*
stset futime, failure(status==1) scale(365.25)
stcompet CIF_trt=ci, compet1(2) by(trtf)
gen CIF_trt_dead=CIF_trt if status==1
gen CIF_trt_relapse=CIF_trt if status==2

twoway (line prob1_trt _t if trtf==1, sort lcolor(navy) lpattern(dash)) ///
	   (line prob2_trt _t if trtf==1, sort lcolor(red) lpattern(dash)) ///
	   (line CIF_trt_dead _t if trtf==1, sort lcolor(navy) lpattern(solid)) ///
	   (line CIF_trt_relapse _t if trtf==1, sort lcolor(red) lpattern(solid)), ///
	   ytitle("Probability of Death") ///
	   xtitle("Time since diagnosis, years") ///
	   legend(order(1 "Death K-M" 2 "Relapse K-M" 3 "Death CIF" 4 "Relapse CIF")) ///
	   ylabel(0(0.2)1.0, angle(0) format(%3.1f)) xlabel(0(1)7) ///
	   title("Treatment A") ///
	   name(KM_CIF_TrtA, replace)
graph export "Writeup\Graphs\Myeloid\KM_CIF_TrtA.png", replace
	   
twoway (line prob1_trt _t if trtf==2, sort lcolor(navy) lpattern(dash)) ///
	   (line prob2_trt _t if trtf==2, sort lcolor(red) lpattern(dash)) ///
	   (line CIF_trt_dead _t if trtf==2, sort lcolor(navy) lpattern(solid)) ///
	   (line CIF_trt_relapse _t if trtf==2, sort lcolor(red) lpattern(solid)), ///
	   ytitle("Probability of Death") ///
	   xtitle("Time since diagnosis, years") ///
	   legend(order(1 "Death K-M" 2 "Relapse K-M" 3 "Death CIF" 4 "Relapse CIF")) ///
	   ylabel(0(0.2)1.0, angle(0) format(%3.1f)) xlabel(0(1)7) ///
	   title("Treatment B") ///
	   name(KM_CIF_TrtB, replace)
graph export "Writeup\Graphs\Myeloid\KM_CIF_TrtB.png", replace	   


graph combine KM_CIF_TrtA KM_CIF_TrtB, title("Cumulative incidence and complement of Kaplan-Meier" "for time to relapse or death")
graph export "Writeup\Graphs\Myeloid\KM_CIF_Trts.png", replace	   

* Two cause-specific survival models
capture drop tt
range tt 0 7 1001

* Death
stset futime, failure(status=1) id(id) scale(365.25)
stpm2 trtf, df(3) scale(hazard) eform
estimates store death
predict hd_A, hazard timevar(tt) at(trtf 1) per(1000)
predict hd_B, hazard timevar(tt) at(trtf 2) per(1000)

twoway	(line hd_? tt) ///
	,ytitle("Mortality rate (per 1000 py)") ///
	xtitle("Time since diagnosis, years") ///
	title("Death without relapse") name(Death, replace) ///
	legend(order(1 "Treatment A" 2 "Treatment B") cols(1))
	graph export "Writeup\Graphs\Myeloid\Death.png", replace
	
* Relapse
stset futime, failure(status=2) id(id) scale(365.25)
stpm2 trtf, df(3) scale(hazard) eform
estimates store relapse
predict hr_A, hazard timevar(tt) at(trtf 1) per(1000)
predict hr_B, hazard timevar(tt) at(trtf 2) per(1000)

twoway	(line hr_? tt) ///
	,ytitle("Relapse rate (per 1000 py)") ///
	xtitle("Time since diagnosis, years") ///
	title("Relapse") name(Relapse, replace) ///
	legend(order(1 "Treatment A" 2 "Treatment B") cols(1))
	graph export "Writeup\Graphs\Myeloid\Relapse.png", replace

graph combine Death Relapse, ycommon xcommon name(haz_deathrelapse, replace) ///
title("Hazard predicted under flexible parametric models")
graph export "Writeup\Graphs\Myeloid\Haz_DeathRelapse.png", replace
/* Predict cause-specific CIFs
predictms, models(death relapse) cr aj timevar(tt) ///
at1(trtf 1) at2(trtf 2)

line _prob_at* tt, title("Treatment A") name(TrtA, replace) ///
lpattern(solid solid solid dash dash dash) ///
legend(label(1 ) label(2 ) label(3 ) ///
label(4 ) label(5 ) label(6 ))
line _prob_at2* tt, title("Treatment B") name(TrtB, replace) nodraw
graph combine TrtA TrtB*/


* Use predictms to obtain the cumulative incidence functions
predictms, models(relapse death) timevar(tt) cr aj ci ///
	at1(trtf 1) at2(trtf 2)
rename _prob_at1_1_1* P_alive_TrtA*
rename _prob_at2_1_1* P_alive_TrtB*
rename _prob_at1_1_2* CIF_relapseTrtA*
rename _prob_at2_1_2* CIF_relapseTrtB*
rename _prob_at1_1_3* CIF_deathTrtA*
rename _prob_at2_1_3* CIF_deathTrtB*

* plot empirical vs model estimates
twoway (line CIF_relapseTrtA tt, sort lcolor(red) lpattern(solid)) ///
       (line CIF_deathTrtA tt, sort lcolor(navy) lpattern(solid)) /// 
	   (line CIF_trt_relapse _t if trtf==1, sort lcolor(red) lpattern(dash)) ///
	   (line CIF_trt_dead _t if trtf==1, sort lcolor(navy) lpattern(dash)) ///
	   ,ytitle("Probability of Death") title("Treatment A") ///
	   xtitle("Time since diagnosis, years") ///
	   legend(order(1 "Relapse" 2 "Death without relapse") cols(1)) ///
	   ylabel(0(0.2)0.8, angle(0) format(%3.1f)) name(TrtAfpm, replace)
	   
twoway (line CIF_relapseTrtB tt, sort lcolor(red) lpattern(solid)) ///
       (line CIF_deathTrtB tt, sort lcolor(navy) lpattern(solid)) /// 
	   (line CIF_trt_relapse _t if trtf==2, sort lcolor(red) lpattern(dash)) ///
	   (line CIF_trt_dead _t if trtf==2, sort lcolor(navy) lpattern(dash)) ///
	   ,ytitle("Probability of Death") title("Treatment B") ///
	   xtitle("Time since diagnosis, years") ///
	   legend(order(1 "Relapse" 2 "Death without relapse") cols(1)) ///
	   ylabel(0(0.2)0.8, angle(0) format(%3.1f)) name(TrtBfpm, replace)

graph combine TrtAfpm TrtBfpm, ycommon xcommon name(FPM, replace) ///
title("Cause-specific CIF predicted under flexible parametric models" "compared to non-parametric estimates")
graph export "Writeup\Graphs\Myeloid\FPM_CIF.png", replace

* Stacked plots
gen TrtA_total1=CIF_relapseTrtA
gen TrtA_total2=TrtA_total1 + CIF_deathTrtA

gen TrtB_total1=CIF_relapseTrtB
gen TrtB_total2=TrtB_total1 + CIF_deathTrtB

twoway (area TrtA_total2 tt, sort fintensity(100)) ///
	   (area TrtA_total1 tt, sort fintensity(100)), ///
	    ylabel(0(0.2)1, angle(0) format(%3.1f)) ///
		ytitle("Probability of Event") xtitle("Time since diagnosis, years") ///
		legend(order(2 "Relapse" 1 "Death without relapse") size(small) cols(1)) ///
        title("Treatment A") plotregion(margin(zero)) name(TrtAstack, replace)
		
twoway (area TrtB_total2 tt, sort fintensity(100)) ///
	   (area TrtB_total1 tt, sort fintensity(100)), ///
	    ylabel(0(0.2)1, angle(0) format(%3.1f)) ///
		ytitle("Probability of Event") xtitle("Time since diagnosis, years")  ///
		legend(order(2 "Relapse" 1 "Death without relapse") size(small) cols(1)) ///
        title("Treatment B") plotregion(margin(zero)) name(TrtBstack, replace)

graph combine TrtAstack TrtBstack, ycommon xcommon name(FPMstack, replace) ///
title("Stacked probability of relapse and death without relapse")
graph export "Writeup\Graphs\Myeloid\FPMstack.png", replace
