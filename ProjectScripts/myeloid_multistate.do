* STATA DO FILE
* Project
* Multistate course - Acute myeloid leukaemia data
* 04/06/2019

* set Stata version
version 15.1

* start logging
capture log using "log.log", append

* set working directory
capture cd "C:\Users\infin\Documents\Leicester\Project"

********************************************************************************
* 
*-------------------------------------------------------------------------------
* Load in the data
*-------------------------------------------------------------------------------
use "Data\myeloid", clear

list if txtime == crtime & txtime != .
* One instance where remission and transplant take place at same time
* It seems probable that remission occurred before it was confirmed
* to avoid clashes, set remission a day earlier
replace crtime = crtime - 1 if id == 428

* Make treatment a factor
encode trt, gen(trtf)
levels(trtf)

* Gen event indicators
gen transplant = txtime != .
gen remission = crtime != .
gen relapse = rltime != .

graph twoway (line txtime id) (line crtime id) (line rltime id) 

* Straight from diagnosis to death
count if death==1 & remission==0 & transplant==0 & relapse==0
* Diagnosis -> Remission -> Death
count if death==1 & remission==1 & transplant==0 & relapse==0 ///
	& crtime < futime
* Diagnosis -> Remission -> Transplant -> Death
count if death==1 & remission==1 & transplant==1 & relapse==0 ///
	& crtime < txtime & txtime < futime
* Diagnosis -> Remission -> Relapse -> Death
count if death==1 & remission==1 & transplant==0 & relapse==1 ///
	& crtime < rltime & rltime < futime
* Diagnosis -> Remission -> Transplant -> Relapse -> Death
count if death==1 & remission==1 & transplant==1 & relapse==1 ///
	& crtime < rltime & rltime < txtime & txtime < futime


*-------------------------------------------------------------------------------
* Investigate the canonical cancer->remission->transplant->relapse->death model
*-------------------------------------------------------------------------------

* Status column: 0 "Cancer" 1 "Remission" 2 "Transplant" 3 "Relapsed" 4 "Dead"
capture drop status
gen status = crtime != .
replace status = 2 if txtime !=.
replace status = 3 if rltime !=.
replace status = 4 if death
/*capture drop status
egen maxtime = rowmax(crtime txtime rltime)
gen status = 0
replace status = 1 if crtime == maxtime
replace status = 2 if txtime == maxtime
replace status = 3 if rltime == maxtime
replace status = 4 if death*/

label define statlbl 0 "Cancer" 1 "Remission" 2 "Transplant" 3 "Relapsed" 4 "Dead"
label values status statlbl
tab status
/*label define statlbl 0 "Cancer" 1 "Remission" 2 "Transplant" 3 "Relapse" 4 "Dead"
label values status statlbl
tab status*/

* prepare transition matrix
*mat tmat = (.,1,2,.,3\.,.,4,5,6\.,.,.,7,8\.,.,.,.,9\.,.,.,.,.)
*mat list tmat
/*mat tmat = (.,1,2,.,3\.,.,4,5,6\.,7,.,.,8\.,.,9,.,10\.,.,.,.,.)
mat list tmat*/

* prepare data
msset, id(id) transmat(tmat) states(remission transplant relapse death) ///
times(crtime txtime rltime futime) //covariates(trtf)
matrix tmat = r(transmatrix)

* Count how many patients underwent each transition
matrix freqmat = r(freqmatrix)
mat list freqmat
local rdim = rowsof(freqmat)
local cdim = colsof(freqmat) - 1

capture file close latrix
file open latrix using "Writeup\Tables\latrix.tex", write replace
// header
file write latrix "$$\begin{pmatrix}" _newline 
forvalues i = 1/`rdim' {
	forvalues j = 1/`cdim' {
		local val = freqmat[`i',`j']
		display `val'
		file write latrix "`val' & " 
	}
	local lastval = freqmat[`i', 5]
	file write latrix "`lastval' \\" _newline
}
file write latrix "\end{pmatrix}$$"
file close latrix

* Declare survival data
stset _stop, enter(_start) failure(_status=1) scale(365.25)



* Create visual summary
msboxes, transmat(tmat) id(id)  xvalues(0.1 0.3 0.7 0.9 0.5) yvalues(0.5 0.9 0.9 0.5 0.1) ///
			boxheight(0.15) boxwidth(0.15) statenames("Diagnosis" "Remission" "Transplant" "Relapse" "Death")
graph export "Writeup\Graphs\Myeloid\MultiFlow.png", replace

* Calculate and plot the non-parametric Aalen-Johansen estimates of transition probabilities
msaj, transmat(tmat) id(id)
twoway (line P_AJ* _t,sort connect(stepstair)) , 		///
		title("Aalen-Johanessen estimates of transition probabilities")		///
		legend(order(1 "Post diagnosis" 2 "Complete remission" 3 "Transplant" 4 "Relapse" 5 "Death"))	///
		xtitle("Follow-up time (years)") ytitle("Transition probability ")
graph export "Writeup\Graphs\Myeloid\AJ_1.png", replace

* Predict the cumulative hazard function using proportional and non-proportional hazards
* Weibull models
capture drop ch*
stpm2 _trans2 _trans3 _trans4 _trans5 _trans6 _trans7, scale(h) df(1)
predict ch1, cumhazard
stpm2 _trans2 _trans3 _trans4 _trans5 _trans6 _trans7, scale(h) df(1) ///
tvc(_trans2 _trans3 _trans4 _trans5 _trans6 _trans7) dftvc(1)
predict ch2, cumhazard

* Predict each transition with a separate Weibull model
// Fit exponential model for each transition 
forvalues i = 1/9 { 
	display "Transition `i'" 
	streg if _trans == `i', dist(exp) nolog noshow 
	estimates store m_exp`i'
}

// Fit stpm2 model with 3 df for each transition 
forvalues i = 1/9 {
	display "Transition `i'" 
	stpm2 if _trans==`i', scale(hazard) df(3) lininit
	estimates store m_rp`i'
}


* Predict the Nelson-Aalen estimate of the cumulative hazard function
capture drop na
sts gen na = na, by(_trans)

twoway 	(line ch1 na _t if _trans==1,sort lcol(red red) connect(join stepstair) lpat(dash))	///
		(line ch1 na _t if _trans==2,sort lcol(blue blue) connect(join stepstair) lpat(dash))	///
		(line ch1 na _t if _trans==3,sort lcol(green green) connect(join stepstair) lpat(dash))	///
		(line ch1 na _t if _trans==4,sort lcol(purple purple) connect(join stepstair) lpat(dash))	///
		(line ch1 na _t if _trans==5,sort lcol(orange orange) connect(join stepstair) lpat(dash))	///
		(line ch1 na _t if _trans==5,sort lcol(brown brown) connect(join stepstair) lpat(dash))	///
		(line ch1 na _t if _trans==5,sort lcol(yellow yellow) connect(join stepstair) lpat(dash))	///
		, name(g1,replace) title("Proportional model")  ///
		legend(order(1 "Diagnosis -> Remission" 3 "Diagnosis -> Death" ///
		5 "Remission -> Transplant" 7 "Remission -> Death" ///
		9 "Transplant -> Relapse" 11 "Transplant -> Death" 13 "Relapse -> Death") cols(1))

twoway 	(line ch2 na _t if _trans==1,sort lcol(red red) connect(join stepstair) lpat(dash))	///
		(line ch2 na _t if _trans==2,sort lcol(blue blue) connect(join stepstair) lpat(dash))	///
		(line ch2 na _t if _trans==3,sort lcol(green green) connect(join stepstair) lpat(dash))	///
		(line ch2 na _t if _trans==4,sort lcol(purple purple) connect(join stepstair) lpat(dash))	///
		(line ch2 na _t if _trans==5,sort lcol(orange orange) connect(join stepstair) lpat(dash))	///
		(line ch2 na _t if _trans==5,sort lcol(brown brown) connect(join stepstair) lpat(dash))	///
		(line ch2 na _t if _trans==5,sort lcol(yellow yellow) connect(join stepstair) lpat(dash))	///
		, name(g2,replace) title("Stratified model") nodraw ///
		legend(order(1 "Diagnosis -> Complete Remission" 3 "Diagnosis -> Death" 5 "Complete Remission -> Relapse" 7 "Complete Remission -> Death" 9 "Relapse - Death") cols(1))

graph combine g1 g2
graph export "Writeup\Graphs\Myeloid\CumuHaz.png", replace

* Choose best model
capture est drop m*
forvalues i=1/7 {
	forvalues df=1/9 {
		capture qui stpm2 if _trans==`i', scale(h) df(`df') failconvlininit
		if _rc != 0 {
			di "m`i'_`df' did not converge"
		} 
		else {
			est store m`i'_`df'
		}
	}
	qui count if _trans==`i'& _d==1
	est stats m`i'_*, n(`r(N)')
}

stpm2 if _trans==3, scale(h) df(7) failconvlininit

********************************************************************************
********************************************************************************
********************************************************************************
********************************************************************************

//final models
stpm2 if _trans==1, scale(h) df(8) failconvlininit
est store m1
stpm2 if _trans==2, scale(h) df(2) 
est store m2
stpm2 if _trans==3, scale(h) df(3)
est store m3

//We find that we need much higher degrees of freedom for the transition between post-transplant and platelet recovery (transition 1), due to the 
//complex nature of the hazard rate, i.e it contains turning points.

//(j) For your chosen models, recalculate the cumulative hazard and overlay on the Nelson-Aalen estimate. Comment on the fit.

cap drop ch*
est restore m1
predict ch1, cumhazard 
est restore m2
predict ch2, cumhazard  
est restore m3
predict ch3, cumhazard  

twoway 	(line ch1 na _t if _trans==1,sort lcol(red red) connect(join stepstair) lpat(dash))	///
		(line ch2 na _t if _trans==2,sort lcol(blue blue) connect(join stepstair) lpat(dash))	///
		(line ch3 na _t if _trans==3,sort lcol(green green) connect(join stepstair) lpat(dash))	///
		, name(g1,replace) title("Best fitting models") ///
		legend(order(1 "Transplant -> Platelet recovery" 3 "Transplant -> Relapse/death" 5 "Platelet recovery -> relapse/death") cols(1))
//the fitted values appear to capture the cumulative hazard function extremely well, showing we have have sufficient flexibility in our baseline models

//(k) Use msaj and predictms to calculate transition probabilities, both non-parametrically and from your 
//fitted survival models. Graphically compare them.

capture range temptime 0 7 1000
predictms, transmat(tmat) models(m1 m2 m3) timevar(temptime) seed(74314)
twoway (line P_AJ* _t,sort connect(stairstep))(line _prob_at1* temptime,sort)
//We find extremely good agreement between estimated transition probabilities

//(l) Now consider the covariates available, build your own transition-specific models. Consider investigating 
//non-proportional hazards.

//transition 1: post-transplant to platelet recovery
stpm2 ds2 ds3 ag2 ag3 drm2 tc2 if _trans==1, scale(h) df(8) failconvlininit
est store m1

//transition 2: post-transplant to relapse/death
stpm2 ds2 ds3 ag2 ag3 drm2 tc2 if _trans==2, scale(h) df(2) 
est store m2

//transition 3: platelet recovery to relapse/death
stpm2 ds2 ds3 ag2 ag3 drm2 tc2 if _trans==3, scale(h) df(3)
est store m3

//(m) Use predictms to show the impact of a covariate of your choice, for example across three age groups
predictms , transmat(tmat) models(m1 m2 m3) timevar(temptime)	///
			at1(ds2 1 drm2 1) 			///
			at2(ds2 1 drm2 1 ag2 1)		///
			at3(ds2 1 drm2 1 ag3 1)		///
			difference ratio ci	 		///
			seed(74314)					///
			m(50)						//reduced for computation time for teaching purposes

twoway (rarea _diff_prob_at2_1_3_lci _diff_prob_at2_1_3_uci temptime)(line _diff_prob_at2_1_3 temptime)	///
		, name(g1,replace) title("Age 20-40 compared to Age<20") legend(off) xtitle("Follow-up time (years)")

twoway (rarea _diff_prob_at3_1_3_lci _diff_prob_at3_1_3_uci temptime)(line _diff_prob_at3_1_3 temptime)	///
		, name(g2,replace) title("Age >40 compared to Age<20") legend(off) xtitle("Follow-up time (years)")
		
graph combine g1 g2, xcommon ycommon title("Difference in probability of relapse/death")

//This nicely shows the impact of age, in that unsurprisingly, a higher age at transplant increases your 
//probability of relapse/death
		
//(n) The previous models have been Markov models, assuming that where you are going next is not influenced by where 
//you have come from. In our setting the probability of relapse/death following platelet recovery could be 
//influenced by the time of platelet recovery. Assess this by altering your transition 3 model.

//we can use our time of entry _t0, which is the time of platelet recovery.
//refit transition 3 model, adjusting for platelet recovery time
stpm2 _t0 ds2 ds3 age drm2 tc2 if _trans==3, scale(h) df(3)

//we get a pvalue of 0.996 for the effect of time of platelet recovery on the rate of relapse/death 
//following platelet recovery, showing no evidence that it influences this transition.

//BONUS

//(o) Compare a prediction of your choice for different simulation sample sizes n(). How important is n?

//for the same n, we can do 5 runs to see the impact of simulation
cap gen time5 = 0 in 1
replace time5 = 5 in 2
cap gen prob1 = .
cap gen prob2 = .
cap gen prob3 = .
set seed 12345
forval i=1/5 {
	predictms , transmat(tmat) models(m1 m2 m3) timevar(time5) at1(ds2 1 drm2 1) n(1000)
	forval j=1/3 {
		qui replace prob`j' = _prob_at1_1_`j'[2] in `i'
	}
	local i = `i' + 1
}
list prob1 prob2 prob3 in 1/5
//now change to 100,000
set seed 12345
forval i=1/5 {
	predictms , transmat(tmat) models(m1 m2 m3) timevar(time5) at1(ds2 1 drm2 1) n(100000)
	forval j=1/3 {
		qui replace prob`j' = _prob_at1_1_`j'[2] in `i'
	}
	local i = `i' + 1
}
list prob1 prob2 prob3 in 1/5

//we can vary n to see the impact
cap gen n = .
local i = 1
foreach n in 100 1000 10000 100000 1000000 {
	qui replace n = `n' in `i'
	predictms , transmat(tmat) models(m1 m2 m3) timevar(time5) at1(ds2 1 drm2 1) n(`n')
	forval j=1/3 {
		qui replace prob`j' = _prob_at1_1_`j'[2] in `i'
	}
	local i = `i' + 1
}
list n prob1 prob2 prob3 in 1/5
