capture cd "C:\Users\infin\Documents\Leicester\Project"
* Assign lambda and gamma values:
* Healthy -> Illness
local l11 = 0.2
local g11 = 0.8
local l12 = 1.6
local g12 = 1.0
local p1 = 0.2
local beta1 = 0.4

* Illness -> Death
local l21 = 1.0
local g21 = 1.5
local l22 = 1.0
local g22 = 0.5
local p2 = 0.5
local beta2 = 0.8

* Healthy -> Death
local l31 = 0.03
local g31 = 1.9
local l32 = 0.3
local g32 = 2.5
local p3 = 0.7
local beta3 = 0.6
/*
capture program drop visualise
visualise, seed(398894) ntrans(3) pmix(`p1' `p2' `p3') maxtime(5) ///
	gammas1(`g11' `g21' `g31') lambdas1(`l11' `l21' `l31') ///
	gammas2(`g12' `g22' `g32') lambdas2(`l12' `l22' `l32') ///
 	outdir("Writeup\Graphs\Sim4\") simulation(4) ///
	tde(".8*ln(tt)" ".1*ln(tt)" ".7*ln(tt)") ///
	loghr(`beta1' `beta2' `beta3')

/*
	local collist = ""
	local months = "3 6 12 24 48"
	foreach month of local months {
		foreach state in healthy ill dead {
			local collist = "`collist' `state'_`month'"
		}
	}
	tempname hazfile
	local postdir = "Stata\Simulations\Sim4\outputs\"
	postfile `hazfile' `collist' using `postdir'truehazrat, replace
	local posthaz = ""
	foreach month of local months {
		forvalues tr = 1/3 {
			summ hazrat`tr' if tt==`month'/12
			local hr`tr' = r(mean)
		}
		local posthaz = "`posthaz' (`hr1') (`hr2') (`hr3')"
	}
	di "`posthaz'"
	post `hazfile' `posthaz'
	postclose `hazfile'
*/

illtde, obs(10000000) ntrans(3) maxtime(5) seed(558667) ///
		tde("trt:*.8:*ln(#t)" "trt:*.1:*ln(#t)" "trt:*.7:*ln(#t)") ///
		covar1(0.5 trt `beta1' `beta2' `beta3') ///
		gammas1(`g11' `g21' `g31') lambdas1(`l11' `l21' `l31') ///
		gammas2(`g12' `g22' `g32') lambdas2(`l12' `l22' `l32') ///
		pmix(`p1' `p2' `p3') ///
		truevals(3 6 12 24 48) postdir("Stata\Simulations\Sim4\outputs\")

*/
capture program drop illtde
set seed 558667
forvalues i=1/100 {	
	illtde, obs(1000) ntrans(3) maxtime(5) ///
		tde("trt:*.8:*ln(#t)" "trt:*0.1:*ln(#t)" "trt:*0.7:*ln(#t)") ///
		covar1(0.5 trt `beta1' `beta2' `beta3') ///
		gammas1(`g11' `g21' `g31') lambdas1(`l11' `l21' `l31') ///
		gammas2(`g12' `g22' `g32') lambdas2(`l12' `l22' `l32') ///
		pmix(`p1' `p2' `p3')
		
	save "Stata\Simulations\Sim4\test`i'", replace
}

fitdata, infile("Stata\Simulations\Sim4\test") postdir("Stata\Simulations\Sim4\outputs\") ///
	nruns(100) ntrans(3) maxtime(5) months(3 6 12 24 48) at1(trt 0) at2(trt 1) difference ///
	covariates(trt) ci flexdf(1 2 3 5 8) 

/*
use "Stata\Simulations\Sim4\demo1", clear
gen lnt = ln(_t)
forvalues tr=1/3 {
	stcox trt if _trans == `tr'
	stpm2 trt if _trans == `tr', df(4) scale(h) tvc(trt) dftvc(2)
	est store fp`tr'
}
mat tmat = (.,1,2\.,.,3\.,.,.)
capture drop tt
range tt 0 `maxtime' 10001
predictms, transmatrix(tmat) timevar(tt) models(fp1 fp2 fp3) aj
*/
capture program drop fittde
set trace off
fittde, infile("Stata\Simulations\Sim4\test") ///
	postdir("Stata\Simulations\Sim4\outputs\")  ///
	nruns(100) ntrans(3) maxtime(5) months(3 6 12 24 48) ///
	covariates(trt) at1(trt 0) at2(trt 1) diff ci ///
	flexdf(3 5 8) tvc(trt) tdedf(1 3 5) 



