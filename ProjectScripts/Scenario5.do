capture cd "C:\Users\infin\Documents\Leicester\Project"
version 15.0
* Assign lambda and gamma values:
* Healthy -> Illness
local l11 = 0.2
local g11 = 0.8
local l12 = 1.6
local g12 = 1.0
local p1 = 0.2

* Illness -> Death
local l21 = 1.0
local g21 = 1.5
local l22 = 1.0
local g22 = 0.5
local p2 = 0.5

* Healthy -> Death
local l31 = 0.03
local g31 = 1.9
local l32 = 0.3
local g32 = 2.5
local p3 = 0.7

/*
capture program drop visualise
visualise, seed(398894) ntrans(3) pmix(`p1' `p2' `p3') maxtime(5) ///
	gammas1(`g11' `g21' `g31') lambdas1(`l11' `l21' `l31') ///
	gammas2(`g12' `g22' `g32') lambdas2(`l12' `l22' `l32') ///
 	outdir("Writeup\Graphs\Sim5\") simulation(5) ///
	tde("0.7-1.5*exp(-2*(tt-1.5)^2)" "0.3*ln(tt)" "1.58-8.6*(tt)^0.5+5.75*tt-1.43*tt*ln(tt)") ///
	loghr(0.4 0.8 0.6)
/*
local collist = ""
local months = "3 6 12 24 48"
foreach month of local months {
	foreach state in healthy ill dead {
		local collist = "`collist' `state'_`month'"
	}
}
tempname hazfile
local postdir = "Stata\Simulations\Sim5\outputs\"
postfile `hazfile' `collist' using `postdir'truehazrat, replace
local posthaz = ""
foreach month of local months {
	forvalues tr = 1/3 {
		qui summ hazrat`tr' if tt==`month'/12
		local hr`tr' = r(mean)
	}
	local posthaz = "`posthaz' (`hr1') (`hr2') (`hr3')"
}
post `hazfile' `posthaz'
postclose `hazfile'
*/	

capture  program drop illtde
illtde, obs(10000000) ntrans(3) maxtime(5) seed(558667) ///
		covar1(0.5 trt 0.4 0.8 0.6) ///
		tde("trt:*(0.7:-1.5:*exp(-2:*(#t:-1.5):^2))" "trt:*0.3:*ln(#t)" "trt:*(1.58:-8.6:*(#t):^0.5:+5.75:*#t:-1.43:*#t:*ln(#t))") /// 
		gammas1(`g11' `g21' `g31') lambdas1(`l11' `l21' `l31') ///
		gammas2(`g12' `g22' `g32') lambdas2(`l12' `l22' `l32') ///
		pmix(`p1' `p2' `p3') ///
		truevals(3 6 12 24 48) postdir("Stata\Simulations\Sim5\outputs\")

capture program drop illtde
set seed 558667
forvalues i=1/100 {	
	illtde, obs(1000) ntrans(3) maxtime(5) ///
		covar1(0.5 trt 0.4 0.8 0.6) ///
		tde( "trt:*(0.7:-1.5:*exp(-2:*(#t:-1.5):^2))" "trt:*0.3:*ln(#t)" "trt:*(1.58:-8.6:*(#t):^0.5:+5.75:*#t:-1.43:*#t:*ln(#t))") /// 
		gammas1(`g11' `g21' `g31') lambdas1(`l11' `l21' `l31') ///
		gammas2(`g12' `g22' `g32') lambdas2(`l12' `l22' `l32') ///
		pmix(`p1' `p2' `p3')
		
	//tde("trt:*0.4:*ln(#t)" "trt:*0.8:*(#t):^0.5" "trt:*0.6:*#t")
	//covar1(0.5 trt 0.4 0.8 0.6)
	
	save "Stata\Simulations\Sim5\test`i'", replace
}
*/
fitdata, infile("Stata\Simulations\Sim5\test") postdir("Stata\Simulations\Sim5\outputs\") ///
	nruns(100) ntrans(3) maxtime(5) at1(trt 0) at2(trt 1) covariates(trt) difference ci ///
	months(3 6 12 24 48) flexdf(1 2 3 5 8) 


capture program drop fittde
set trace off
fittde, infile("Stata\Simulations\Sim5\test") ///
	postdir("Stata\Simulations\Sim5\outputs\")  ///
	nruns(100) ntrans(3) maxtime(5) months(3 6 12 24 48) ///
	covariates(trt) at1(trt 0) at2(trt 1) diff ci ///
	flexdf(3 5 8) tvc(trt) tdedf(1 3 5) 



