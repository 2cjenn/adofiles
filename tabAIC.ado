
capture program drop tabAIC
program define tabAIC
	syntax[,					///
			INfile(string)		/// -Name of output file-
			SCENario(int 1)		///
	]
	use `infile', clear
	replace df = df - 1
	
	local ics = "AIC BIC"
	foreach ic of local ics {
		capture frame drop `ic'
		egen min`ic' = min(`ic'), by(run_no trans)
		frame put if `ic'==min`ic', into(`ic')
		
		frame change `ic'
		bys trans: egen mean = mean(df)
		bys trans: egen median = median(df)
		bys trans: egen min = min(df)
		bys trans: egen max = max(df)
		
		frame change default
	}
	
	qui summarize trans
	local ntrans = r(max)
	
	local graphs = ""
	forvalues tr = 1/`ntrans' {
		twoway (scatter AIC df if trans == `tr', msize(small) ///
			title("Transition `tr'") xtitle("Model degrees of freedom") ytitle("AIC") ///
			ylabel(#3, angle(horizontal) labsize(tiny))) ///
		(lowess AIC df if trans == `tr') ///
		, name(AIC`tr', replace) nodraw

		twoway (scatter BIC df if trans == `tr', msize(small) ///
			title("Transition `tr'") xtitle("Model degrees of freedom") ytitle("BIC") ///
			ylabel(#3, angle(horizontal) labsize(tiny))) ///
		(lowess BIC df if trans == `tr') ///
		, name(BIC`tr', replace) nodraw
		
		local graphs = "`graphs' AIC`tr' BIC`tr'"
	}
	grc1leg2 `graphs', ///
			legendfrom(AIC1) rows(3) ysize(5.5) 
	graph export "Writeup\Graphs\Sim`scenario'\ABIC.png", replace
	
	capture file close modelchoice
	file open modelchoice using "`infile'.tex", write replace
	// header
	file write modelchoice	/// 
	"\begin{table}" _newline ///
	"\begin{center}" _newline ///
	"\caption{Comparing the models selected by AIC and BIC for each transition of the simulation. Statistics presented are for the number of degrees of freedom of the chosen model, where 1 d.f. corresponds to the Weibull model and higher degrees are flexible parametric models.}" _newline ///
	"\vspace{5mm}" _newline ///
	"\label{tab:trans`tr'_modelfit}" _newline ///
	"\begin{tabular}{cccccc}" _newline ///
	"\hline" _newline
	file write modelchoice "Transition & Criteria & Mean & Median & Min & Max \\" _newline ///
	"\hline" _newline
	
	forvalues tr = 1/`ntrans' {	
		foreach ic of local ics {
			file write modelchoice "`tr' & `ic'"
			frame change `ic'
			local cols = "mean median min max" 
			foreach col of local cols {
				qui summarize `col' if trans==`tr'
				local val = r(mean)
				local val : display %4.2f `val'
				file write modelchoice "& `val'" 
			}
			file write modelchoice " \\" _newline
			frame change default
		}
	}

	file write modelchoice ///
	"\hline" _newline ///
	"\end{tabular}" _newline ///
	"\end{center}" _newline ///
	"\end{table}" 
	file close modelchoice
	
	foreach ic of local ics {
		frame drop `ic'
	}
end