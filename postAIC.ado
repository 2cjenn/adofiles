capture program drop postAIC
program define postAIC
	syntax[,					///
			POSTing(string)	/// -Directory of post file-
			NTrans(int 1)		///
			MODELS(string)		///
			RUNNO(int 1)		/// -Which simulation run are we on-
	]
	
	
	forvalues i = 1/`ntrans' {
		qui count if _trans==`i'& _d==1
		local n_obs = r(N)
		qui est stats m`i'_*, n(`n_obs')
		matrix stats`i' = r(S)
		local rdim = rowsof(stats`i')
		local cdim = colsof(stats`i')
		
		tokenize "`models'"
		forvalues row = 1/`rdim' {
			local postpar = `"(`runno') (`i') ("``row''")"'
			forvalues col = 1/`cdim' {
				if `col' != 2 {
					local val = stats`i'[`row',`col']
					if `val' != . {
						if (int(`val') != `val') {
							local val : display %8.2f `val'
						}
						local postpar = `"`postpar' (`val')"'
					}
				}
			}
			post `posting' `postpar'
		}
	}
	
end